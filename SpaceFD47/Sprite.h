#pragma once
#include "base.h"
#include "Texture.h"
#include "BaseClass.h"
#include "GlProgram.h"

class Sprite : public BaseClass
{
public:
	Sprite(void);
	Sprite(std::string shader);
	Sprite(std::string Texture, std::string shader, int layer = 0);
	virtual ~Sprite(void);

	virtual void Render();
	virtual void Update(float ticks);

	virtual void Move(glm::vec2 dir);
	virtual void SetPosition(glm::vec2 pos);
	virtual void GetPosition(glm::vec2 &pos);
	virtual void SetOffset(glm::vec4 offset);
	virtual void GetOffset(glm::vec4 &offset);
	virtual void SetSize(glm::vec2 size);
	virtual void GetSize(glm::vec2 &size);
	virtual void SetLayer(int layer);
	virtual int	 GetLayer();

	virtual void SetVisible(bool visible);
	virtual bool GetVisible();

	virtual void				SetTexture(TextureIdent src);
	virtual TextureIdent		GetTexture();
	virtual GlProgram&			GetProgram();
private:
	TextureIdent m_texture;
	bool m_visible;
	int m_layer;
	glm::vec2	m_position; //Where to draw
	glm::vec4	m_offset; //Offset from where to draw. x, y, w, h in UV
	glm::vec2	m_size;  //Size of the drawn area
	
	//Shader-stuff
	GlProgram		m_program; 
			

};

extern BaseClass* MapCreator_Sprite(std::vector<std::string> arguments);
