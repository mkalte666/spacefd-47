/*
File: BaseClass.h
Purpose: Header for the BaseClass. 
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once

#include "base.h"

class BaseClass;

/*
* @name: BaseClass
* @description: BaseClass is an abstract Baseclass for all managed objects in the engine. When created, it registers itself in the scenemanager.
* @note: BaseClass-objects should only created by "new [childclass]" and never deleted directly. insted use "SceneManager::drop(pointer to obj)"
*/
class BaseClass
{
public:
	BaseClass(void);
	virtual ~BaseClass(void);

	virtual void SetSize(glm::vec2 size) {}
	virtual void GetSize(glm::vec2 &size) {}
	virtual void SetPosition(glm::vec2 pos) {}
	virtual void GetPosition(glm::vec2 &pos) {}

	virtual void Update(float timeElapsed);
	virtual void Render();

	virtual uint32_t	GetType();
	virtual void			SetType(uint32_t type);
	
	virtual void	AddSubobject(BaseClass* obj);
	virtual void	FreeSubobject(BaseClass* obj);
	virtual void	DropSubobject(BaseClass* obj);
	virtual std::vector<BaseClass*> GetSubobjects();

	virtual void		SetParent(BaseClass* parentObj);
	virtual BaseClass*	GetParent();
	
private:
	int		m_objId;
	uint32_t m_type;

	//Objects can have subobjects.
	std::vector<BaseClass*> m_subobjects;
	//And parents
	BaseClass*	m_parent;
};

