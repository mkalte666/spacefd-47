#include "BasePlayer.h"


BasePlayer::BasePlayer(std::string name)
: Animation(name, name, "base", 1)
{

}


BasePlayer::~BasePlayer()
{
	Animation::~Animation();
}

void BasePlayer::Update(float timeElapsed)
{

	Animation::Update(timeElapsed);
}

void BasePlayer::SetHealth(float health)
{
	m_health = health;
}

float BasePlayer::GetHealth()
{
	return m_health;
}