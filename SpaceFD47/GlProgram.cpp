#include "GlProgram.h"
#include "glutil.h"
#include "Renderer.h"

std::map<GLuint, std::map<std::string, GLuint>> GlProgram::m_Uniforms = std::map<GLuint, std::map<std::string, GLuint>>();

GlProgram::GlProgram(std::string name)
{
	m_programId = Renderer::GetProgram(name);
	m_name = name;
}

GlProgram::GlProgram(GlProgram&obj)
{
	m_name = obj.GetName();
	m_programId = Renderer::GetProgram(m_name);
}
GlProgram::GlProgram()
{
	m_programId = GL_INVALID_INDEX;
}

GlProgram::~GlProgram()
{
	
}

GLuint GlProgram::operator[](std::string name) {
	//This is dirty but should do what i want.
	//-->create a new uniform-element if there is none
	if (m_Uniforms[m_programId].find(name) == m_Uniforms[m_programId].end())
		m_Uniforms[m_programId][name] = glGetUniformLocation(m_programId, name.c_str());
	return m_Uniforms[m_programId][name];
}

void GlProgram::Use()
{
	glUseProgram(m_programId);
}

std::string GlProgram::GetName()
{
	return m_name;
}