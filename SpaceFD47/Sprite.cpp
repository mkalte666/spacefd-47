#include "Sprite.h"
#include "masterutil.h"
#include "Renderer.h"
#include "SceneManager.h"

Sprite::Sprite(void)
: m_program("")
{

}

Sprite::Sprite(std::string program)
: m_program(program)
{
}

Sprite::Sprite(std::string texture, std::string program, int layer)
: m_position(0, 0), m_offset(0, 0, 1, 1), m_size(1, 1), m_program(program)
{
	m_texture = SceneManager::GetTexture(texture);
	m_visible = true;
	m_layer = layer;
	
}

Sprite::~Sprite(void)
{
	BaseClass::~BaseClass();
}

void Sprite::Render()
{
	if(!m_visible) return;
	Renderer::Draw(m_texture, m_program, &m_position[0], &m_size[0], &m_offset[0], m_layer);
}

void Sprite::Update(float ticks)
{

}

void Sprite::Move(glm::vec2 dir) 
{
	m_position += dir;
}

void Sprite::SetPosition(glm::vec2 pos)
{
	m_position = pos;
}

void Sprite::GetPosition(glm::vec2 &pos)
{
	pos = m_position;
}


void Sprite::SetOffset(glm::vec4 offset)
{
	m_offset = offset;
}

void Sprite::GetOffset(glm::vec4 &offset)
{
	offset = m_offset;
}


void Sprite::SetSize(glm::vec2 size)
{
	m_size = size;
}

void Sprite::GetSize(glm::vec2 &size)
{
	size = m_size;
}

void Sprite::SetVisible(bool visible)
{
	m_visible = visible;
}

bool Sprite::GetVisible()
{
	return m_visible;
}

void Sprite::SetTexture(TextureIdent src)
{
	m_texture = src;
}

TextureIdent Sprite::GetTexture()
{
	return m_texture;
}

GlProgram& Sprite::GetProgram()
{
	return m_program;
}

void Sprite::SetLayer(int layer)
{
	m_layer = layer;
}

int Sprite::GetLayer()
{
	return m_layer;
}

BaseClass* MapCreator_Sprite(std::vector<std::string> arguments)
{
	if (arguments.size() != 3) {
		std::cout << "ERROR in sprite MapCreator - invalid arguments" << std::endl;
		return NULL;
	}

	std::string tex, shader;
	int layer;
	stringstream layerstream(arguments[2]);
	layerstream >> layer;
	int beginpos, endpos;
	beginpos = endpos = 0;
	//texture argument
	beginpos = arguments[0].find_first_not_of(" \"");
	endpos = arguments[0].find_last_not_of(" \"");
	if (endpos == arguments[0].npos || beginpos == arguments[0].npos) {
		std::cout << "ERROR in sprite MapCreator - invalid arguments" << std::endl;
		return NULL;
	}
	tex = arguments[0].substr(beginpos, endpos - beginpos + 1);
	//shader argument
	beginpos = arguments[1].find_first_not_of(" \"");
	endpos = arguments[1].find_last_not_of(" \"");
	if (endpos == arguments[1].npos || beginpos == arguments[1].npos) {
		std::cout << "ERROR in sprite MapCreator - invalid arguments" << std::endl;
		return NULL;
	}
	shader = arguments[1].substr(beginpos, endpos - beginpos + 1);
	return new Sprite(tex, shader, layer);
}