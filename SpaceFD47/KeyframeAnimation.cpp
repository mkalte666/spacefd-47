#include "KeyframeAnimation.h"
#include "setting.h"
#include "SceneManager.h"
#include "Interaction.h"

KeyframeAnimation::KeyframeAnimation(std::string name, bool loop, bool dropAtEnd, BaseClass* parent)
: m_DropOnEnd(dropAtEnd), m_shouldLoop(loop)
{
	if (parent != nullptr)
		parent->AddSubobject(this);
	setting<std::string> keyframeAnimationDirSetting("animationdir");
	std::string keyframeAnimationPath = keyframeAnimationDirSetting.GetSettingFast() + name + ".keyanim";
	ifstream infile(keyframeAnimationPath, std::ios::in);
	char buffer[BUFFER_SIZE];
	if (infile.is_open()) {
		while (!infile.eof()) {
			infile.getline(buffer, BUFFER_SIZE);
			std::string line = buffer;
			int pos = 0;
			if ((pos = line.find_first_not_of(" ")) != line.npos && line[pos] == '#') //skip comment lines
				continue;
			if (line[0] == '\n')	//skip emptylines
				continue;

			std::string command;
			int commandEndpos = 0;
			if ((commandEndpos = line.find_first_of(" \n;", pos)) != line.npos)
			{
				command = line.substr(pos, commandEndpos - pos );
			}
			//Keyframe-command. creates a keyframe. If its the first in the list, its the beginning-keyframe and the last one in the list will be set
			//as the last one (for control magic and maybe future extensions)
			if (command.find("keyframe") != command.npos) {
				keyframe newKeyframe;
				newKeyframe.bScales = newKeyframe.bMoves = false;
				std::string args = line.substr(commandEndpos, line.length() - commandEndpos);
				while (args.find_first_of(",;") != args.npos) {
					pos = args.find_first_not_of(" ");
					int endpos = 0;
					if ((endpos = args.find_first_of("=", pos)) == args.npos) {
						std::cout << "ERROR: '" << keyframeAnimationPath << "': Syntax error in \"" << line << "\"!" << std::endl;
						break;
					}
					std::string type, arg1, arg2;
					type = args.substr(pos, endpos - pos);
					//time=duration
					if (type.find("time") != type.npos) {
						int seperator = args.find_first_of(" \n;,", endpos);
						if (seperator != args.npos) {
							arg1 = args.substr(endpos + 1, seperator - endpos - 1);
							std::stringstream OutStream(arg1);
							OutStream >> newKeyframe.blendTime;
							args = args.substr(seperator + 1, args.length() - seperator - 1);
							
						}
					}
					//x= position
					else if (type.find("x") != type.npos) {
						int seperator = args.find_first_of(" \n;,", endpos);
						if (seperator != args.npos) {
							arg1 = args.substr(endpos + 1, seperator - endpos);
							std::stringstream OutStream(arg1);
							OutStream >> newKeyframe.pos.x;
							args = args.substr(seperator + 1, args.length() - seperator - 1);
							newKeyframe.bMoves = true;
							
						}
					}
					//y= position
					else if (type.find("y") != type.npos) {
						int seperator = args.find_first_of(" \n;,", endpos);
						if (seperator != args.npos) {
							arg1 = args.substr(endpos + 1, seperator - endpos);
							std::stringstream OutStream(arg1);
							OutStream >> newKeyframe.pos.y;
							args = args.substr(seperator + 1, args.length() - seperator - 1);
							newKeyframe.bMoves = true;
							
						}
					}
					//w= size
					else if (type.find("w") != type.npos) {
						int seperator = args.find_first_of(" \n;,", endpos);
						if (seperator != args.npos) {
							arg1 = args.substr(endpos + 1, seperator - endpos);
							std::stringstream OutStream(arg1);
							OutStream >> newKeyframe.size.x;
							args = args.substr(seperator + 1, args.length() - seperator - 1);
							newKeyframe.bScales = true;
							
						}
					}
					//h= size
					else if (type.find("h") != type.npos) {
						int seperator = args.find_first_of(" \n;,", endpos);
						if (seperator != args.npos) {
							arg1 = args.substr(endpos + 1, seperator - endpos);
							std::stringstream OutStream(arg1);
							OutStream >> newKeyframe.size.y;
							args = args.substr(seperator + 1, args.length() - seperator - 1);
							newKeyframe.bScales = true;
							
						}
					}
				}
				//Add the new keyframe to the list
				if (m_keyframes.size() == 0)
					newKeyframe.isStart = !(newKeyframe.isEnd = false);
				else
					newKeyframe.isStart = newKeyframe.isEnd = false;
				m_keyframes.push_back(newKeyframe);
			}

			//Should this animation loop?
			else if (command.find("loop") != command.npos) {
				m_shouldLoop = true;
			}

			//Should this animation be dropped after it finsihed?
			else if (command.find("dropAfterFinish") != command.npos) {
				m_DropOnEnd = true;
			}
		}
		if (m_keyframes.size() != 0) {
			m_keyframes.rbegin()->isEnd = true;
		}
		m_currentKeyframe = m_keyframes.begin();
		
	}
	m_current_timer.Reset();
}

void KeyframeAnimation::Update(float timeElapsed)
{
	BaseClass::Update(timeElapsed);

	//If keyframes are invalid  return and TODO: be intelligent and do something sensefull
	if (m_keyframes.begin() == m_keyframes.end())
		return;
	
	//Some booleans as readable-helpres
	bool bShouldMove = m_currentKeyframe->bMoves;
	bool bShouldScale = m_currentKeyframe->bScales;

	

	//If we are the first keyframe, just set the position and go on. our blend-time is only interesting if were looping
	if (m_currentKeyframe->isStart) {
		if (GetParent()!=nullptr) {
			if (bShouldMove) {
				GetParent()->SetPosition(m_currentKeyframe->pos);
			}
			if (bShouldScale)
				GetParent()->SetSize(m_currentKeyframe->size);
			
		}
		m_currentKeyframe++;

		glm::vec2 posToTarget, pos;
		glm::vec2 scaleToTarget, size;
		if (GetParent() != nullptr) {
			//Pos speed
			GetParent()->GetPosition(pos);
			posToTarget = m_currentKeyframe->pos - pos;
			m_currentKeyframe->movementSpeed = posToTarget.length() / m_currentKeyframe->blendTime;
			//Scaling speed
			GetParent()->GetSize(size);
			scaleToTarget = m_currentKeyframe->size - size;
			m_currentKeyframe->ScalingSpeed = scaleToTarget.length() / m_currentKeyframe->blendTime;
		}
		m_current_timer.Reset();
	}
	//If not, go on
	else {
		//Have we reachend the blend time? Set us to where we want to be! Oh and start over if looping or drop if deleting is set
		if (m_currentKeyframe->blendTime <= m_current_timer.Get()) {
			/*if (GetParent()!=nullptr) {
				if (bShouldMove) {
					GetParent()->SetPosition(m_currentKeyframe->pos);
				}
				if (bShouldScale)
					GetParent()->SetSize(m_currentKeyframe->size);

			}*/

			m_currentKeyframe++;
			m_current_timer.Reset();

			//Are were at end-iterator? lets go back to the biginning... just to make shure
			if (m_currentKeyframe == m_keyframes.end()) {
				if (m_DropOnEnd) {
					SceneManager::Drop(GetParent());
					SENDMESSAGE_CLASS_DESTROY(GetParent())
					return;
				}
				else if (m_shouldLoop) {
					m_currentKeyframe = m_keyframes.begin();
					m_current_timer.Reset();
				}
			}

			//Set the speed etc.
			glm::vec2 posToTarget, pos;
			glm::vec2 scaleToTarget, size;
			if (GetParent() != nullptr) {
				//Pos speed
				GetParent()->GetPosition(pos);
				posToTarget = m_currentKeyframe->pos - pos;
				m_currentKeyframe->movementSpeed = posToTarget.length() / m_currentKeyframe->blendTime;
				//Scaling speed
				GetParent()->GetSize(size);
				scaleToTarget = m_currentKeyframe->size - size;
				m_currentKeyframe->ScalingSpeed = scaleToTarget.length() /  m_currentKeyframe->blendTime;
			}

			return;
		}

		//If were in a movement, move all subobjects. the movementspeed btw is calculated every frame and the moved distance etc.
		float timeRemaining = m_currentKeyframe->blendTime - m_current_timer.Get();
		if (bShouldMove) {
			glm::vec2 posToTarget, pos;
			if (GetParent() != nullptr) {
				GetParent()->GetPosition(pos);
				posToTarget = m_currentKeyframe->pos - pos;
				float len = sqrt(posToTarget.x*posToTarget.x + posToTarget.y*posToTarget.y);
				if (len != 0.00000) {
					glm::normalize(posToTarget);
					posToTarget = posToTarget * m_currentKeyframe->movementSpeed * timeElapsed;
					pos += posToTarget;
					GetParent()->SetPosition(pos);
				}
			}
		}
		if (bShouldScale) {
			glm::vec2 scaleToTarget, size;
			if (GetParent() != nullptr) {
				GetParent()->GetSize(size);
				scaleToTarget = m_currentKeyframe->size - size;
				float len = sqrt(scaleToTarget.x*scaleToTarget.x + scaleToTarget.y*scaleToTarget.y);
				if (len != 0.00000) {
					glm::normalize(scaleToTarget);
					scaleToTarget = scaleToTarget * m_currentKeyframe->ScalingSpeed * timeElapsed;
					size += scaleToTarget;
					GetParent()->SetSize(size);
				}
			}
		}

	}
}

KeyframeAnimation::~KeyframeAnimation()
{
	BaseClass::~BaseClass();
}


void KeyframeAnimation::AttatchToObject(BaseClass* obj)
{
	if (GetParent() != nullptr)
		GetParent()->FreeSubobject(this);

	obj->AddSubobject(this);
}