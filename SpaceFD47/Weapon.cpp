#include "Weapon.h"
#include "masterutil.h"
#include "setting.h"

Weapon::Weapon(const char* name, Player* parent)
	: Player(name), m_parent(parent)
{
	
	//REGISTER_OBJ((void*)this);
	setting<std::string>keyfile("keybinds");
	setting<int> primkey("primary_attack", keyfile.GetSettingFast().c_str());
	setting<int> seckey("secondary_attack", keyfile.GetSettingFast().c_str());
	m_attackKey = primkey.GetSettingFast();
	m_secondaryKey = seckey.GetSettingFast();


	m_primaryTimer = TIME().RegisterTimer(m_primaryTime, false);
	m_secondaryTimer = TIME().RegisterTimer(m_secondaryTime, false);
}


Weapon::~Weapon(void)
{
	
}

void Weapon::Update(float m_timeElapsed)
{
	if(m_parent==NULL) return;
	
	glm::vec2 pos;
	m_parent->GetPosition(pos);
	SetPosition(pos);
	
	//Attack Management
	if(MOUSE_KEYSTATE(m_attackKey))
		PrimaryAttack();
	if(MOUSE_KEYSTATE(m_secondaryKey))
		SecondaryAttack();
}

void Weapon::PrimaryAttack()
{
	if(TIME().GetTimer(m_primaryTimer).m_running) return;


	TIME().RemoveTimer(m_primaryTimer);
	m_primaryTimer = TIME().RegisterTimer(m_primaryTime, true);
}

void Weapon::SecondaryAttack()
{
	if(TIME().GetTimer(m_secondaryTimer).m_running) return;


	TIME().RemoveTimer(m_secondaryTimer);
	m_secondaryTimer = TIME().RegisterTimer(m_secondaryTime, true);
}

