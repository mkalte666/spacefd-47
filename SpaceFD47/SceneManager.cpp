#include "SceneManager.h"
#include "Text.h"

std::vector<BaseClass*>	SceneManager::m_SceneElements = std::vector<BaseClass*>();
std::vector<BaseClass*>	SceneManager::m_DropElements = std::vector<BaseClass*>();
std::vector<Texture*>	SceneManager::m_Textures = std::vector<Texture*>();
TimeManager					SceneManager::m_time = TimeManager();
Timer				SceneManager::m_masterTimer = Timer();
Timer				SceneManager::m_frameTimer = Timer();
int						SceneManager::m_generated_num = 0;


SceneManager::SceneManager(void) 
{
}

SceneManager::~SceneManager(void)
{
}

void SceneManager::Render()
{
	//TODO: add more preparing here!
	if( m_SceneElements.size() <= 0 ) return;
	
	for(unsigned int i = 0; i < m_SceneElements.size(); i++) {
		if(m_SceneElements[i]==NULL) continue;
		m_SceneElements[i]->Render();
	}
	
}

void SceneManager::Update()
{
	//static Text *tmp = new Text("1234567890", "Orbitracer", "text", glm::vec2(0, 0.9), 0.1f);
	static int counter = 0;
	m_time.Update();
	glfwPollEvents();
	//tmp->SetColor(glm::vec4(1.0));


	//Drop elements that want to be dropped
	//In case elements get dropped in destructors, we copy our vector an clear the orginal, so we dont get in trouble with iterators
	//Then loop until no elements are marked to drop
	while (m_DropElements.size() != 0) {
		std::vector<BaseClass*>  DropElements = m_DropElements;
		m_DropElements.clear();
		for (auto i = DropElements.begin(); DropElements.size()!=0 && i != DropElements.end(); i++) {
			RemoveObject(*i);
			delete *i;
		}
		DropElements.clear();
	}

	for (unsigned int i = 0; i < m_SceneElements.size(); i++) {
		if (m_SceneElements[i] == NULL) continue;
		m_SceneElements[i]->Update(static_cast<float>(m_masterTimer.Get()));
	}

	Render();
	if (counter >= 5) {
		std::stringstream frametime;
		frametime << int(1.0f / m_masterTimer.Get());
		//tmp->SetText(frametime.str());
		counter = 0;
	}
	counter++;
	m_masterTimer.Reset();

}

Texture* SceneManager::GetTexture(std::string name)
{
	//standart format is .png
	name += ".png";
	for(unsigned int i=0; i < m_Textures.size(); i++) {
		if(name.compare(m_Textures[i]->GetName()) == 0) {
			return m_Textures[i];
		}
	}
	//If it is not loaded yet, load it!
	
	m_Textures.push_back(new Texture(name));
	m_Textures[m_Textures.size()-1]->Create();
	return m_Textures[m_Textures.size()-1];
}

//Used to let the user manage textures themself. only the pointer gets registerd to the scene-manager
Texture* SceneManager::GenTexture(Texture* texture)
{
	texture->Create();
	m_Textures.push_back(texture);
	m_generated_num++;
	return texture;
}

void SceneManager::DelTexture(Texture* texture)
{
	for (std::vector<Texture*>::iterator pos = m_Textures.begin(); pos != m_Textures.end(); pos++) {
		if (*pos == texture) {
			pos = m_Textures.erase(pos);
		}
	}

	delete texture;
}

int SceneManager::AddObject(BaseClass &obj)
{
	//Search if we have empty places...
	for(unsigned int i=0; i < m_SceneElements.size(); i++) {
		if(m_SceneElements[i]==NULL) {
			m_SceneElements[i] = &obj;
			return i;
		}
	}

	//If there is no free space in the array, push back the address as a new element.
	m_SceneElements.push_back(&obj);
	return m_SceneElements.size()-1;
}

void SceneManager::RemoveObject(unsigned int obj)
{
	if(obj>=m_SceneElements.size()) return;
	m_SceneElements[obj] = NULL;
}

void SceneManager::RemoveObject(BaseClass* obj)
{
	for(unsigned int i=0; i < m_SceneElements.size(); i++) {
		if(obj==m_SceneElements[i]) {
			m_SceneElements[i] = NULL;
		}
	}
}

TimeManager& SceneManager::GetTime()
{
	return m_time;
}

bool SceneManager::VerifyObject(BaseClass* obj)
{
	//Maybe this is to slow cause its called frequently by the physics.
	std::vector<BaseClass*>::iterator iter = m_SceneElements.begin();
	for(;iter!=m_SceneElements.end();iter++)
		if(*iter == obj) return true;
	return false;
	

	

}

void SceneManager::Init()
{
	m_masterTimer.Reset();
	m_frameTimer.Reset();
	return;
}

void SceneManager::Drop(BaseClass* obj)
{
	if (VerifyObject(obj))
		m_DropElements.push_back(obj);
}

std::vector<BaseClass*> SceneManager::GetAllObjects()
{
	return m_SceneElements;
}