#pragma once
#include "base.h"

struct SoundSource;
typedef unsigned int SoundSourceIdent;

class Sound
{
public:
	Sound();
	~Sound();
	static bool Init();
	static void Destroy();
	
	static void SetListenerPos(glm::vec3 pos);
	static void SetListenerVel(glm::vec3 v);

	static SoundSourceIdent AddSource(SoundSource s);
	static SoundSource&		GetSource(SoundSourceIdent ident);
	static void				DelSource(SoundSourceIdent ident);
	static ALuint			GetSoundBuffer(std::string sound);
private:
	//Al basic stuff
	static ALCdevice		*m_device;
	static ALCcontext		*m_context;
	static std::list<SoundSource> m_sources;
	static unsigned long	m_maxSouceId;
	static std::map<std::string, ALuint> m_sounds;
};

struct SoundSource
{
	glm::vec3 pos = glm::vec3(0,0,0);
	glm::vec3 vel = glm::vec3(0,0,0);
	float	pitch = 1.0f;
	float   gain = 0.6f;
	bool	looping = AL_FALSE;
	bool	paused = true;
	ALuint	buffer = 0;
	ALuint source = 0;

	SoundSourceIdent id = 0;

	void Load(std::string name);
	void Play();
	void Cleanup();
};