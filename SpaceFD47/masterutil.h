/*
File: masterutil.h
Purpose: Contains the static Engine-class
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once

#include "base.h"

typedef void(*GenericCallback)(void*, void*);

/*
* @name: Engine
* @desctiption: Class that contains the Basic management functions for the Engine
*/
class Engine
{
public:

	static bool Init();
	
	static void RegisterCallback(GenericCallback callback, std::string ident, void* arg1 = NULL);
	static void RunCallback(std::string ident, void* pArg = NULL);
	static void RemoveCallback(std::string ident);
	static FT_Library GetFontSystem();

private:

	static bool m_init;
	static FT_Library m_ftLibary;

	static std::map<std::string, GenericCallback> *m_callbacks;
	static std::map<std::string, void*> *m_callbackArgs;
protected:
	static void Loop();
	static void Startup();
	static void Cleanup();
};

/*
* @name: Callback
* @description: Callback-structor that, when initialised as global varaiable, add functions to the callback system.
*/
class Callback
{
public:
	Callback(GenericCallback callback, const char* ident, void* arg1 = NULL);
	~Callback();
	void Run();
private:
	std::string m_ident;
};
