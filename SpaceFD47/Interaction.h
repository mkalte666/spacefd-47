#pragma once
#include "base.h"
#include "BaseClass.h"

//Interaction types
enum ACTION
{
	//no args
	ACTION_NONE = 0,

	//Basic Actions

	/*ACTION_ATTACK: 
	fArg = amount of damage
	iArg = type of damage
	*/
	ACTION_ATTACK,
	
	//no args
	ACTION_COLLIDE,
	
	/*ACTION_DESTROY
	iArg = type of destruction
	*/
	ACTION_DESTROY,

	/*ACTION_REMOVED
	*/
	ACTION_DESTROYED,

	//Player and AI specific
	
	/*ACTION_PLAYER_DESTROYED
	*/
	ACTION_PLAYER_DESTROYED,
	
	/*ACTION_PLAYER_MOVED
	posArg: position of the player
	*/
	ACTION_PLAYER_MOVED,

	LAST_ACTION
};

//Scene Types. Elements have TYPE_NORMAL as standart
enum TYPE
{
	TYPE_NORMAL	= 1,
	TYPE_NPC	= 2,
	TYPE_PLAYER = 4,
	TYPE_BULLET = 8,
	TYPE_ALL	= 15	
};

struct Message
{
	float				fArg		= 0.0f;
	int					iArg		= 0;
	bool				bArg		= false;
	glm::vec2			posArg		= glm::vec2(0);
	void*				pArg		= NULL;
	ACTION				type		= ACTION_NONE;
	BaseClass*			source		= NULL;
	BaseClass*			destination = NULL;
	bool				isBrodcast	= false;
	unsigned int		maxFrames	= 1;
};

class MessageManager
{
public:
	static void Init();
	static void Send(Message msg);
	static void Brodcast(Message msg);
	static void Update();
	static bool PollMessage(BaseClass*dst, Message&msg);
	static bool CopyListSilent(BaseClass* dst, std::list<Message> &dstlist);
	
private:
	static std::map<BaseClass*, std::list<Message>> m_messages;
};

#define SENDMESSAGE_CLASS_DESTROY(obj) \
	Message destroymsg;	\
	destroymsg.source = destroymsg.destination = obj; \
	destroymsg.type = ACTION_DESTROYED; \
	MessageManager::Brodcast(destroymsg);