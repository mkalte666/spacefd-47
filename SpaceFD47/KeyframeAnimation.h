#pragma once
#include "BaseClass.h"
#include "time.h"


struct keyframe
{
	glm::vec2	pos;
	glm::vec2	size;
	bool		bScales;
	bool		bMoves;
	float		blendTime;
	bool		isStart;
	bool		isEnd;

	//Set dynamicly by KeyframeAnimation
	float		movementSpeed;
	float		ScalingSpeed;
	glm::vec2	movementDir;
	glm::vec2	scalingDir;
};

class KeyframeAnimation : BaseClass
{
public:
	KeyframeAnimation(std::string name, bool loop = false, bool dropAtEnd = false, BaseClass* parent=nullptr);
	~KeyframeAnimation();

	virtual void Update(float timeElapsed) override;
	
	virtual void AttatchToObject(BaseClass* obj);
private:
	std::list<keyframe> m_keyframes;
	bool				m_shouldLoop;
	bool				m_DropOnEnd;
	Timer				m_current_timer;
	std::list<keyframe>::iterator	m_currentKeyframe;
};

