/*
File: Animation.h
Purpose: Contains Class for Animations. 
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once
#include "Sprite.h"
#include "time.h"

/* 
* @name: Animation
* @parent: Sprite
* @description: Creates an animation from a animation.anim file. 
*/
class Animation :
	public Sprite
{
public:
	Animation(std::string animation, std::string texture, std::string shader, int layer=0);
	virtual ~Animation(void);

	virtual void Render();
	virtual void Update(float timeElapsed);
	virtual void Run(float time=0.0);
	virtual void Stop();

private:
	std::vector<std::pair<int, float>> m_frames;
	std::vector<std::pair<int, float>>::iterator m_activeFrame;
	int	m_tilesW;
	int m_tilesH;
	int m_tilenum;
	float m_framenum;
	bool m_drawable;
	bool m_running;
	Timer m_timer;
	Timer m_frameTimer;
	float m_timeToRun;
	
};

