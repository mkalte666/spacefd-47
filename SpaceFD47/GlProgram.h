#pragma once
#include "base.h"
class GlProgram
{
public:
	GlProgram();
	GlProgram(std::string name);
	GlProgram(GlProgram &obj);
	~GlProgram();

	GLuint	operator[](std::string);
	void Use();
	std::string GetName();

private:
	GLuint							m_programId;
	static std::map<GLuint, std::map<std::string, GLuint>>	m_Uniforms;
	std::string m_name;
};

