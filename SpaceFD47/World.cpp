/*
File: World.cpp
Purpose: Contains the functions for the World-class
Author(s): Malte Kie�ling (mkalte666)
*/
#include "World.h"


World::World(void)
{
}


World::~World(void)
{
	BaseClass::~BaseClass();
}


void World::SetGravity(glm::vec2 gravity) 
{
	m_gravity = gravity;
}

void World::GetGravity(glm::vec2&gravity)
{
	gravity = m_gravity;
}

void World::SetWorldUnits(glm::vec2 units)
{
	m_world_units = units;
}

void World::GetWorldUnits(glm::vec2&units)
{
	units = m_world_units;
}

void World::SetAmbientColor(glm::vec4 color)
{
	m_ambientLight = color;
}

void World::GetAmbientColor(glm::vec4&color)
{
	color = m_ambientLight;
}

void World::SetAmbientStrength(float strength)
{
	m_ambientStrength = strength;
}

float World::GetAmbientStrength()
{
	return m_ambientStrength;
}