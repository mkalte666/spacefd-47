#pragma once
#include "window.h"
#include "Renderer.h"
class UI
{
public:
	static bool Init(window* wnd);
	static void Destroy();

	static bool GetKey(int keycode);
	static bool GetMousebutton(int keycode);
	static void GetCursor(double&x, double&y);
	static void SetCursor(double x, double y);

	static void Update();
private:
	UI(window* wnd);
	~UI();
private:
	static window* m_window;
};

