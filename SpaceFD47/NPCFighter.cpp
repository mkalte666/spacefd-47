#include "NPCFighter.h"
#include "setting.h"
#include "Interaction.h"
#include "SceneManager.h"
#include "Projectile.h"

NPCFighter::NPCFighter(std::string name)
: BasePlayer(name)
{
	setting<std::string> npcdir("npcdir");
	std::string settingfile(npcdir.GetSetting());
	settingfile += name + ".cfg";
	SetPosition(glm::vec2(0.5, 0.5));

	setting<float> raw_boud("collision", settingfile);
	setting<float> size("size", settingfile);
	setting<float> mass("mass", settingfile);
	setting<float> health("health", settingfile);
	setting<float> offset("weaponOffset", settingfile);

	std::vector<float> offsetArray = offset.GetSettingArray();
	m_weaponOffset = glm::vec2(offsetArray[0], offsetArray[1]);

	SetHealth(health.GetSettingFast());
	SetSize(glm::vec2(size.GetSettingFast(), size.GetSettingFast()));

	m_physical = Physics::GetActiveSystem().AddObj(new CollisionPolygon(raw_boud, this), this);
	m_physical->mass = mass.GetSettingFast();

	m_shootDir = glm::vec2(0.0, -1.0);
	m_testTimer = SceneManager::GetTime().RegisterTimer(0.3f, true);
	SetType(TYPE_NPC);
}


NPCFighter::~NPCFighter()
{
	Physics::GetActiveSystem().RemoveObj(this);
	BasePlayer::~BasePlayer();
}

void NPCFighter::Update(float timeElapsed)
{
	glm::vec2 size, pos;
	GetSize(size);
	GetPosition(pos);

	Message msg;
	while (MessageManager::PollMessage(this, msg)) {
		switch (msg.type)
		{
		case ACTION_ATTACK:
			SetHealth(GetHealth() - msg.fArg);
			break;
		case ACTION_PLAYER_MOVED:	//For test we do ~something~, 
			//TODO: add AI actions
			m_shootDir = msg.posArg - (pos + m_weaponOffset*size);
			glm::normalize(m_shootDir);
			break;
		default:
			break;
		}
	}

	if (GetHealth() <= 0.0f) {
		SceneManager::Drop(this);
		Animation* explosion = new Animation("explosion_1", "explosion_1", "base", 1);
		explosion->SetSize(size);
		explosion->SetPosition(pos);
		explosion->Run(1.0f);
		SENDMESSAGE_CLASS_DESTROY(this)
		
	}
	if (!m_testTimer->m_running) {
		PrimaryAttack();
		SceneManager::GetTime().RemoveTimer(m_testTimer);
		m_testTimer = SceneManager::GetTime().RegisterTimer(0.8f, true);
	}

	BasePlayer::Update(timeElapsed);
}

void NPCFighter::PrimaryAttack()
{
	glm::vec2 pos, vel, size;
	GetSize(size);
	GetPosition(pos);
	pos += m_weaponOffset*size;
	vel = m_shootDir;
	vel *= 0.1;
	if (vel.y<0)
	new Projectile("base_projectile", pos, vel);

}

BaseClass* MapCreator_NPCFighter(std::vector<std::string> arguments)
{
	if (arguments.size() != 1) {
		std::cout << "Error in NPCFighter creation: invalid arguments" << std::endl;
		return NULL;
	}
	std::string name(arguments[0]);
	int beginpos, endpos;
	if ((beginpos = name.find_first_not_of("\"")) == name.npos) {
		std::cout << "Error in NPCFighter creation: invalid arguments" << std::endl;
		return NULL;
	}
	name = name.substr(beginpos, name.length() - beginpos);
	if ((endpos = name.find_first_of("\"")) == name.npos) {
		std::cout << "Error in NPCFighter creation: invalid arguments" << std::endl;
		return NULL;
	}
	name = name.substr(0, endpos);

	NPCFighter * newnpc = new NPCFighter(name);
	newnpc->Run();
	return newnpc;
}