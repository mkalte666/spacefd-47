/*
File: alutil.h
Purpose: Header for the OpenAL abstraction
Author(s): Malte Kie�ling (mkalte666)
*/

#pragma once
#include "base.h"

extern ALuint	GenerateBufferFromWAV(std::string file);
extern ALuint GenerateBufferFromFile(std::string filename);