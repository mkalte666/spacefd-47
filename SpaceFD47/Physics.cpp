/*
File: Physics.cpp
Purpose: Contains the functions of the Physics.h
Author(s): Malte Kie�ling (mkalte666)
*/
#include "Physics.h"
#include "masterutil.h"
#include "SceneManager.h"

Physics* Physics::m_active = NULL;

void PhysicsThread(Physics* p)
{
	//Rate set in define
	while (true) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1000 / 60));
		p->ActualUpdate(1.0 / 60.0);
	}
}

Physics::Physics(void)
: m_objects(0) 
{
	m_updateThread = std::thread(PhysicsThread, this);
}


Physics::~Physics(void)
{
	m_updateThread.detach();
}




void Physics::ActualUpdate(float TimeElapsed)
{
	//wait while changes happen
	m_mutex.lock();

	if (this != m_active) {
		m_active->Update(TimeElapsed);
		m_mutex.unlock();
		return;
	}
	std::vector<PhysicalObj*>::iterator i;
	for (i = m_objects.begin(); i < m_objects.end(); i++) {
		//Update skipTicks for eatch object with skipTicks
		if ((*i)->skipTicks>0) {
			(*i)->skipTicks--;
			continue;
		}
		/*if (!SceneManager::VerifyObject((*i)->obj)) {
			i = m_objects.erase(i);
			if (i == m_objects.end()) break;
			continue;
		}*/
		glm::vec2 pos1;
		(*i)->obj->GetPosition(pos1);
		std::vector<PhysicalObj*>::iterator j;
		for (j = i+1; j < m_objects.end(); j++) {
			if ((*j)->skipTicks > 0) {
				//we dont update skipticks here - every obj would be updated twice!
				continue;
			}
			/*if (!SceneManager::VerifyObject((*j)->obj)) {
				j = m_objects.erase(j);
				if (j == m_objects.end()) break;
				continue;
			}*/
			glm::vec2 pos2;
			(*j)->obj->GetPosition(pos2);
			if ((*i)->collider->Collide(*(*j)->collider)) {
				
				Message	message1;
				Message  message2;
				message1.source = (*i)->obj;
				message2.source = (*j)->obj;
				message1.type = message2.type = ACTION::ACTION_COLLIDE;
				message1.destination = (*j)->obj;
				message2.destination = (*i)->obj;


				float m1 = (*i)->mass;
				float m2 = (*j)->mass;
				glm::vec3 v1((*i)->v,0);
				glm::vec3 v2((*j)->v, 0);
				glm::vec3 v1New;
				glm::vec3 v2New;
				glm::vec3 dist((pos2 - pos1), 0);
				
				if (dist.x < EPSILON && dist.y < EPSILON) {
					//v1New = v2;
					//v2New = v1;
				} else
				 {
					glm::vec3 a = glm::normalize(dist);
					glm::vec3 n;
					n = glm::cross(v1, v2);
					if (n.x == n.y && n.x == n.z && n.x == 0.0f) {
						v1New = ((m1 - m2)*v1 + 2 * m2 * v2) / (m1 + m2);
						v2New = ((m2 - m1)*v2 + 2 * m1 * v1) / (m1 + m2);
					}
					else {
						n = glm::normalize(n);
						glm::vec3 b = glm::cross(n, a);
						float ab = glm::dot(a, b);

						float v11 = (glm::dot(v1, a) - glm::dot(v1, b)*ab) / (1 - ab*ab);
						float v12 = (glm::dot(v1, b) - glm::dot(v1, a)*ab) / (1 - ab*ab);
						float v21 = (glm::dot(v2, a) - glm::dot(v2, b)*ab) / (1 - ab*ab);
						float v22 = (glm::dot(v2, b) - glm::dot(v2, a)*ab) / (1 - ab*ab);

						v1New = a * ((m1 - m2)*v11 + 2 * m2*v21) / (m1 + m2) + b*v12;
						v2New = a * ((m2 - m1)*v21 + 2 * m1*v11) / (m1 + m2) + b*v22;
					}
				}
				

				//Update stuff if the collision groups match
				if ((*j)->obj->GetType() & (*i)->collision_type) {
					(*i)->v = glm::vec2(v1New.x, v1New.y);
					MessageManager::Send(message1);
					
				}
				if ((*i)->obj->GetType() & (*j)->collision_type) {
					(*j)->v = glm::vec2(v2New.x, v2New.y);
					MessageManager::Send(message2);
				}
				
				//After a collision an objects will skip a small amout of phx tics to avoid objects to get stuck in each other
				(*i)->skipTicks = 1;
				(*j)->skipTicks = 1;
			}
		}
	}

	for (i = m_objects.begin(); i < m_objects.end(); i++) {
		/*if (!SceneManager::VerifyObject((*i)->obj)) {
			i = m_objects.erase(i);
			if (i == m_objects.end()) break;
			continue;
		}*/
		glm::vec2 pos;
		(*i)->obj->GetPosition(pos);
		pos += (*i)->v*TimeElapsed;
		(*i)->v += (*i)->a*TimeElapsed;
		(*i)->obj->SetPosition(pos);
	}
	m_mutex.unlock();
}

PhysicsIdent Physics::AddObj(CollisionPolygon *collider, BaseClass* obj, uint32_t type)
{
	m_mutex.lock();
	PhysicalObj *newobj = new PhysicalObj();
	newobj->a = glm::vec2(0);
	newobj->v = glm::vec2(0);
	newobj->obj = obj;
	newobj->collider.reset(collider);
	newobj->collision_type = type;
	m_objects.push_back(newobj);
	m_mutex.unlock();
	return newobj;
}

bool Physics::RemoveObj(BaseClass* obj)
{
	m_mutex.lock();
	for (auto i = m_objects.begin(); i < m_objects.end(); i++) {
		if ((*i)->obj == obj) {
			i=m_objects.erase(i);
			m_mutex.unlock();
			return true;
		}
	}
	return false;
	m_mutex.unlock();
}

void Physics::SetWorld(World&world) 
{
	m_mutex.lock();
	m_world = world;
	m_mutex.unlock();
}

void Physics::GetWorld(World&world)
{
	m_mutex.lock();
	world = m_world;
	m_mutex.unlock();
}

Physics& Physics::GetActiveSystem()
{

	return *m_active;
}

void Physics::SetAsActiveSystem()
{
	m_mutex.lock();
	m_active = this;
	m_mutex.unlock();
}