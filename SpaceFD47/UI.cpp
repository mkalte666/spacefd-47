#include "UI.h"

window* UI::m_window = NULL;

UI::UI(window* wnd)
{
	m_window = wnd;
}


UI::~UI()
{
	m_window = 0;
}

bool UI::Init(window* wnd)
{
	m_window = wnd;
	return true;
}

void UI::Destroy()
{
	if (!m_window) return;
	m_window = NULL;
}

bool UI::GetKey(int keycode)
{
	if (m_window == NULL) return false;
	return m_window->GetKeyState(keycode);
}

bool UI::GetMousebutton(int button)
{
	if (m_window == NULL) return false;
	return m_window->GetMouseButtonState(button);
}

void UI::GetCursor(double&x, double&y)
{
	if (m_window == NULL) return;
	m_window->GetCursor(x, y);
}

void UI::SetCursor(double x, double y)
{
	if (m_window == NULL) return;
	m_window->SetCursor(x, y);
}

void UI::Update()
{
	m_window->Update();
}
