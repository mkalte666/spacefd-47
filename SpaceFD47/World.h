/*
File: World.h
Purpose: contains the world-class. This class stores inforations of the phisical enviormnet
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once
#include "BaseClass.h"
#include "base.h"
class World :
	public BaseClass
{
public:
	World(void);
	virtual ~World(void);

	void SetGravity(glm::vec2 gravity);
	void GetGravity(glm::vec2&gravity);
	
	void SetWorldUnits(glm::vec2 units);
	void GetWorldUnits(glm::vec2&units);

	void SetAmbientColor(glm::vec4 color);
	void GetAmbientColor(glm::vec4&color);

	void SetAmbientStrength(float strength);
	float GetAmbientStrength();
private:
	//Gravity and importand stuff
	glm::vec2		m_gravity;		//Stores the Gravity in each directions. [m_gravity] = worldunits/s^2
	glm::vec2		m_world_units;  //Defines the scale of the worldunites. Can be seen as meters. Basically it defines what a meter is in the 0-1 screen range.
	
	//Ambient
	glm::vec4		m_ambientLight; //RGB with the ambientlighting-color
	float			m_ambientStrength;

	//
};

