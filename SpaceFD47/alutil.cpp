/*
File: alutil.cpp
Purpose: Contains Abstraction functions for ffmpeg-audio reading and some openAL functions
Author(s): Malte Kie�ling (mkalte666)
*/

#include "alutil.h"

/*
* @name: GenerateBufferFromWAV
* @return: ALuint - ALuint that represents the loaded audio buffer
* @param:	std::string file: name of the pcm-wave
* @description: Function loads a pcm-wave file with the name in "file". 
* @notes: GenerateBufferFromFile should be used insted!
*/
ALuint GenerateBufferFromWAV(std::string file)
{
	//Generate buffer
	ALuint buffer;
	alGenBuffers(1, &buffer);
	
	ifstream infile(file, std::ios::binary);
	char* header = new char[44];
	infile.read(header, 44);
	uint16_t channels = *(uint16_t*)(header + 0x16);
	uint32_t rate = *(uint32_t*)(header + 0x18);
	uint16_t framesize = *(uint16_t*)(header + 0x20);
	uint32_t datasize = (*(uint32_t*)(header + 0x04))+8-44;
	uint32_t fmtsite = (*(uint32_t*)(header + 0x10));
	unsigned char* data = new unsigned char[datasize];
	infile.seekg(0x2c);
	infile.read((char*)data, datasize);
	alBufferData(buffer, AL_STEREO16_SOFT, data, datasize, rate);
	delete[] data;
	delete[] header;
	return buffer;
}

/*
* @name: fdata
* @description: holder for char* buffers with size. used by GenerateBufferFromFile
*/
struct fdata
{
	int size;
	char *data;
};

/*
* @name: GenerateBufferFromFile
* @return: ALuibnt that represents the loaded audio-buffer
* @param: std::string filename: name of the file to be read
* @description: loads a audiofile with the help of ffmpeg.
* @notes: ffmpeg can load many, but bot all aud�o codecs. Please view its documentation for more information
*/
ALuint GenerateBufferFromFile(std::string filename)
{
	AVFrame* frame = av_frame_alloc();
	if (!frame)
	{
		std::cout << "Error allocating the frame" << std::endl;
		return 1;
	}

	// you can change the file name "01 Push Me to the Floor.wav" to whatever the file is you're reading, like "myFile.ogg" or
	// "someFile.webm" and this should still work
	AVFormatContext* formatContext = NULL;
	if (avformat_open_input(&formatContext, filename.c_str(), NULL, NULL) != 0)
	{
		av_free(frame);
		std::cout << "Error opening the file" << std::endl;
		return 1;
	}

	if (avformat_find_stream_info(formatContext, NULL) < 0)
	{
		av_free(frame);
		avformat_close_input(&formatContext);
		std::cout << "Error finding the stream info" << std::endl;
		return 1;
	}

	// Find the audio stream
	AVCodec* cdc = nullptr;
	int streamIndex = av_find_best_stream(formatContext, AVMEDIA_TYPE_AUDIO, -1, -1, &cdc, 0);
	if (streamIndex < 0)
	{
		av_free(frame);
		avformat_close_input(&formatContext);
		std::cout << "Could not find any audio stream in the file" << std::endl;
		return 1;
	}

	AVStream* audioStream = formatContext->streams[streamIndex];
	AVCodecContext* codecContext = audioStream->codec;
	codecContext->codec = cdc;

	if (avcodec_open2(codecContext, codecContext->codec, NULL) != 0)
	{
		av_free(frame);
		avformat_close_input(&formatContext);
		std::cout << "Couldn't open the context with the decoder" << std::endl;
		return 1;
	}

	std::cout << "This stream has " << codecContext->channels << " channels and a sample rate of " << codecContext->sample_rate << "Hz" << std::endl;
	std::cout << "The data is in the format " << av_get_sample_fmt_name(codecContext->sample_fmt) << std::endl;

	AVPacket readingPacket;
	av_init_packet(&readingPacket);

	//codecContext->

	std::vector<fdata> bufferdata;
	// Read the packets in a loop
	while (av_read_frame(formatContext, &readingPacket) == 0)
	{
		if (readingPacket.stream_index == audioStream->index)
		{
			AVPacket decodingPacket = readingPacket;

			// Audio packets can have multiple audio frames in a single packet
			while (decodingPacket.size > 0)
			{
				
				// Try to decode the packet into a frame
				// Some frames rely on multiple packets, so we have to make sure the frame is finished before
				// we can use it
				int gotFrame = 0;
				av_frame_unref(frame);
				int result = avcodec_decode_audio4(codecContext, frame, &gotFrame, &decodingPacket);

				if (result >= 0 && gotFrame)
				{
					decodingPacket.size -= result;
					decodingPacket.data += result;

					// We now have a fully decoded audio frame
					unsigned int datasize = static_cast<unsigned int>(av_samples_get_buffer_size(NULL, codecContext->channels, frame->nb_samples, codecContext->sample_fmt, 1));
					char* bdata = new char[datasize];
					//If we have planar types, we have a problem. then the channeldata are in data[0] and data[1]
					//We want them in one buffer (c1 c1 c2 c2 c1 c1 c2 c2 for 16 bit stereo planar f.e), so we have to convert
					unsigned int bytespersample = static_cast<unsigned int>(av_get_bytes_per_sample(codecContext->sample_fmt));
					unsigned int outbufpos = 0;
					switch (codecContext->sample_fmt)
					{
					case AV_SAMPLE_FMT_S16P:
					case AV_SAMPLE_FMT_S32P:
					case AV_SAMPLE_FMT_U8P:
					case AV_SAMPLE_FMT_FLTP:
					case AV_SAMPLE_FMT_DBLP:
						for (unsigned int i = 0; i < datasize / codecContext->channels; i += codecContext->channels) {
							for (unsigned int chan = 0; chan < static_cast<unsigned int>(codecContext->channels); chan++) {
								for (unsigned int curbyte = 0; curbyte < bytespersample; curbyte++) {
									bdata[outbufpos] = frame->data[chan][i+curbyte];
									outbufpos++;
								}
							}
						}
						break;
					default:
						memcpy(bdata, frame->data[0], datasize);
						break;
					}
					fdata tmp;
					tmp.data = bdata;
					tmp.size = datasize;
					bufferdata.push_back(tmp);
				}
				else
				{
					decodingPacket.size = 0;
					decodingPacket.data = nullptr;
				}
			}
		}

		// You *must* call av_free_packet() after each call to av_read_frame() or else you'll leak memory
		av_free_packet(&readingPacket);
	}

	// Some codecs will cause frames to be buffered up in the decoding process. If the CODEC_CAP_DELAY flag
	// is set, there can be buffered up frames that need to be flushed, so we'll do that
	if (codecContext->codec->capabilities & CODEC_CAP_DELAY)
	{
		av_init_packet(&readingPacket);
		// Decode all the remaining frames in the buffer, until the end is reached
		int gotFrame = 0;
		while (avcodec_decode_audio4(codecContext, frame, &gotFrame, &readingPacket) >= 0 && gotFrame)
		{
			unsigned int datasize = static_cast<unsigned int>(av_samples_get_buffer_size(NULL, codecContext->channels, frame->nb_samples, codecContext->sample_fmt, 1));
			char* bdata = new char[datasize];
			unsigned int bytespersample = static_cast<unsigned int>(av_get_bytes_per_sample(codecContext->sample_fmt));
			unsigned int outbufpos = 0;
			switch (codecContext->sample_fmt)
			{
			case AV_SAMPLE_FMT_S16P:
			case AV_SAMPLE_FMT_S32P:
			case AV_SAMPLE_FMT_U8P:
			case AV_SAMPLE_FMT_FLTP:
			case AV_SAMPLE_FMT_DBLP:
				for (unsigned int i = 0; i < datasize / codecContext->channels; i += codecContext->channels) {
					for (unsigned int chan = 0; chan < static_cast<unsigned int>(codecContext->channels); chan++) {
						for (unsigned int curbyte = 0; curbyte < bytespersample; curbyte++) {
							bdata[outbufpos] = frame->data[chan][i + curbyte];
							outbufpos++;
						}
					}
				}
				break;
			default:
				memcpy(bdata, frame->data[0], datasize);
				break;
			}
			fdata tmp;
			tmp.data = bdata;
			tmp.size = datasize;
			bufferdata.push_back(tmp);
		}
	}

	ALuint result = 0;
	alGenBuffers(1, &result);
	
	long unsigned int offset = 0;
	int datasize = 0;
	for (auto i = bufferdata.begin(); i != bufferdata.end(); i++) {
		datasize += i->size;
	}
	char *resultdata = new char[datasize];
	for (auto i = bufferdata.begin(); i != bufferdata.end(); i++) {
		memcpy_s(&resultdata[offset], i->size, i->data, i->size);
		offset += i->size;
		delete i->data;
	}
	ALenum format = AL_MONO8_SOFT;
	int channels = frame->channels;
	switch (codecContext->sample_fmt)
	{
	case AV_SAMPLE_FMT_S16:
	case AV_SAMPLE_FMT_S16P:
		if (channels == 2) {
			format = AL_STEREO16_SOFT;
		}
		else if (channels == 1) {
			format = AL_MONO16_SOFT;
		}
		else {
			std::cout << "ERROR: Incompatible sample format in " << filename << ": " << codecContext->sample_fmt << " !" << std::endl;
			return 0;
		}
		break;
	case AV_SAMPLE_FMT_S32:
	case AV_SAMPLE_FMT_S32P:
		if (channels == 2) {
			format = AL_STEREO32F_SOFT;
		}
		else if (channels == 1) {
			format = AL_MONO32F_SOFT;
		}
		break;
	default:
		std::cout << "ERROR: Incompatible sample format in " << filename << ": " << codecContext->sample_fmt<< " !" << std::endl;
		return 0;
		break;
	}

	alBufferData(result, format, resultdata, datasize, frame->sample_rate);
	ALenum err = alGetError();
	while (err != AL_NO_ERROR) {
		std::cout << "ERROR IN OPENAL: " << err << " " << std::endl;
	}
	delete[] resultdata;
	// Clean up!
	av_free(frame);
	avcodec_close(codecContext);
	avformat_close_input(&formatContext);
	return result;
}