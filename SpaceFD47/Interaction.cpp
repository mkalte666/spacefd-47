#include "Interaction.h"
#include "SceneManager.h"

std::map<BaseClass*, std::list<Message>> MessageManager::m_messages = std::map<BaseClass*, std::list<Message>>();

void MessageManager::Init()
{

}

void MessageManager::Send(Message msg)
{
	/*if (m_messages.end()==m_messages.find(msg.destination)) {
		m_messages.insert(make_pair(msg.destination, std::list<Message>()));
	}*/
		m_messages[msg.destination].push_back(msg);
}

void MessageManager::Brodcast(Message msg)
{
	msg.isBrodcast = true;
	std::vector<BaseClass*> destinations = SceneManager::GetAllObjects();
	for (auto i = destinations.begin(); i != destinations.end(); i++) {
		if (*i == msg.source)
			continue;
		msg.destination = *i;
		Send(msg);
	}
	return;
}

void MessageManager::Update()
{
	/* MORE UNPERFORMAT THAN YOUR GRANDMA!
	for (auto i = m_messages.begin(); i != m_messages.end(); i++) {
		for (auto j = i->second.begin(); j != i->second.end(); j++) {
			j->maxFrames--;
			if (j->maxFrames < 0)
				j = i->second.erase(j);
		}
	}
	*/
}

bool MessageManager::PollMessage(BaseClass*dst, Message&msg)
{
	bool result = false;
	for (auto i = m_messages[dst].begin(); i != m_messages[dst].end(); i++) {
		if (i->destination == dst) {
			result = true;
			msg = *i;
			i = m_messages[dst].erase(i);
			return result;
		}
	}
	return result;
}

bool MessageManager::CopyListSilent(BaseClass* dst, std::list<Message> &dstlist)
{
	if (m_messages.find(dst) != m_messages.end()) {
		if (m_messages[dst].size() <= 0)
			return false;
		dstlist = m_messages[dst];
		return true;
	}
	return false;
}