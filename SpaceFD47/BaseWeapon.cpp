#include "BaseWeapon.h"


BaseWeapon::BaseWeapon()
{
	BaseClass::~BaseClass();
}


BaseWeapon::~BaseWeapon()
{

}

void BaseWeapon::PrimaryAttack()
{

}

void BaseWeapon::SecondaryAttack()
{

}

void BaseWeapon::Update()
{

}

void BaseWeapon::Equip()
{

}

void BaseWeapon::Drop()
{

}