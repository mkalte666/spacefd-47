/*
File: time.h	
Purpose: Heder for the Time-class. Is responsible for Registering timers, Timer-callbacks, etc.
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once
#include "base.h"

typedef void (*TimerCallback)(float, void*);



struct ManagedTimer
{
	
	float m_initoffset;
	float m_curtime; //Time the timer moved on
	float m_timeelapsed; //Time since last call
	float m_endtime;
	bool m_running;
	bool m_hasCallback;
	TimerCallback callback;
	void* m_callbackArg;
};

typedef ManagedTimer* TimerIdent;

class Timer
{
public:
	Timer();
	~Timer();

	double Get();
	void Reset(double time = 0.00);
private:
	double m_time_started;
};
class TimeManager
{
public:
	TimeManager(void);
	~TimeManager(void);

	void		Update(void);

	TimerIdent	RegisterTimer(bool started=true);
	TimerIdent	RegisterTimer(float timeToEnd, bool started=true);
	TimerIdent	RegisterTimer(TimerCallback callback, float timeToCall, bool started=true);
	TimerIdent	RegisterTimer(TimerCallback callback, float timeToCall, void* arg, bool started=true);
	
	void		RemoveTimer(TimerIdent& ident);

	void		StartTimer(TimerIdent ident);
	void		StopTimer(TimerIdent ident);

private:
	std::list<ManagedTimer*>	m_timers;
	float						m_time;
	float						m_timeElapsed;

protected:
};
