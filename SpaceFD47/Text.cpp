#include "Text.h"
#include "masterutil.h"
#include "setting.h"
#include "Renderer.h"
#include "UI.h"
#include "SceneManager.h"

unsigned char blah[4] = { 255, 0, 255, 125 };

void TextDrawCallback(GlProgram&program, void* arg)
{
	((Text*)arg)->DrawCallback(program);
}

Text::Text(std::string text, std::string font, std::string shader, glm::vec2 pos, float size)
: Sprite(shader)
{
	//Window dim
	int window_w, window_h;
	Renderer::GetWindowDimensions(window_w, window_h);
	//Set text
	m_text = text;
	m_useable = true;
	//Create font path and load font
	setting<std::string> fontdir("fontdir");
	m_fontname = fontdir.GetSettingFast() + font + std::string(".ttf");
	m_TextSize = size;
	SetPosition(pos);
	SetOffset(glm::vec4(0, 0, 1, 1));
	if (FT_New_Face(Engine::GetFontSystem(), m_fontname.c_str(), 0, &m_ftFace)) {
		std::cout << "ERROR: Could not load font!\n";
		m_useable = false;
		return;
	}
	FT_Set_Pixel_Sizes(m_ftFace, 0, static_cast<FT_UInt>(m_TextSize*window_h));
	m_created = false;
	

}


Text::~Text()
{
	FT_Done_Face(m_ftFace);
	Sprite::~Sprite();
}

void Text::Create()
{
	if (!m_useable) return;
	glm::vec2 pos, size;
	glm::vec4 off;
	GetOffset(off);
	GetPosition(pos);
	if (m_letters.size() != 0) {
		for (std::vector<letter>::iterator i = m_letters.begin(); i != m_letters.end(); i++) {
			SceneManager::GenTexture(i->texture);
		}
		m_letters.clear();
	}
	
	m_minpos = m_maxpos = pos;

	//Prepare text: Replace placeholders ('\s' -> ' ', ...)
	std::string::iterator i;
	for (i = m_text.begin(); i != m_text.end(); i++) {
		if (*i == '\\') {
			i = m_text.erase(i);
			if (i != m_text.end()) {
				switch (*i) {
				//Whitespaces can be shown by \s 
				case 's':
					*i = ' ';
					break;

				//Any char with this is x. so we parse the number followed by x, and it has 2 letters (hex, ya know)
				case 'x':
					//TODO: FIX THIS
					*i = '\n'; 
					break;
				}
			}
		}
	}

	//Then create textures. 
	for (i = m_text.begin(); i != m_text.end(); i++) {
		if (FT_Load_Char(m_ftFace, *i, FT_LOAD_RENDER))
			continue;
		
		letter current;
		//Create the texture
		int bitmap_w = m_ftFace->glyph->bitmap.width;
		int bitmap_h = m_ftFace->glyph->bitmap.rows;
		current.texture = new LowFilterTexture(&m_ftFace->glyph->bitmap.buffer[0], bitmap_w, bitmap_h, GL_RED);
		//current.texture = new Texture(&blah[0], 1, 1, GL_RED);

		SceneManager::GenTexture(current.texture);
		int window_w, window_h;
		Renderer::GetWindowDimensions(window_w, window_h);
		float bitmap_scaled_w = (float)bitmap_w / (float)window_w;
		float bitmap_scaled_h = (float)bitmap_h / (float)window_h;
		float bitmap_scaled_offset_x = (float)m_ftFace->glyph->bitmap_left / (float)window_w;
		float bitmap_scaled_offset_y = (float)m_ftFace->glyph->bitmap_top / (float)window_h;
		//move us relative to that etc.
		glm::vec2 letterpos;
		letterpos.x += pos.x + bitmap_scaled_offset_x;
		//positive y us higer so the offset is not negative. cause of the positive offset we have to remove the height of the letter, or it is too much.
		letterpos.y += pos.y + bitmap_scaled_offset_y - bitmap_scaled_h;
		size.x = bitmap_scaled_w;
		size.y = bitmap_scaled_h;
		current.pos = letterpos;
		current.size = size;
		m_letters.push_back(current);

		if (current.pos.x < m_minpos.x)
			m_minpos.x = current.pos.x;
		if (current.pos.y < m_minpos.y)
			m_minpos.y = current.pos.y;
		if (current.pos.x + current.size.x > m_maxpos.x)
			m_maxpos.x = current.pos.x + current.size.x;
		if (current.pos.y + current.size.y > m_maxpos.y)
			m_maxpos.y = current.pos.y + current.size.y;

		pos.x += (float)(m_ftFace->glyph->advance.x >> 6) / (float)window_w;
		pos.y += (float)(m_ftFace->glyph->advance.y >> 6) / (float)window_h;
	}
	m_created = true;

}

void Text::Render()
{
	if (!m_useable) return;
	if (!m_created)
		Create();
	glm::vec4 off;
	GetOffset(off);
	for (std::vector<letter>::iterator i = m_letters.begin(); i != m_letters.end(); i++)
		Renderer::Draw(i->texture, GetProgram(), &i->pos[0], &i->size[0], &off[0], 1, TextDrawCallback, this);
}

void Text::DrawCallback(GlProgram&program)
{
	GLuint color_loc = program["text_color"];
	glUniform4f(color_loc, m_color.r, m_color.g, m_color.b, m_color.a);
}

void Text::GetColor(glm::vec4&color)
{
	color = m_color;
}

void Text::SetColor(glm::vec4 color)
{
	m_color = color;
}

void Text::SetText(std::string text)
{
	m_text = text;
	m_created = false;
}

void Text::GetText(std::string&text)
{
	text = m_text; 
}

bool Text::IsHovered()
{
	if (!m_useable) false;
	double mx, my;
	UI::GetCursor(mx, my);
	if (mx >= m_minpos.x && mx <= m_maxpos.x && my >= m_minpos.y && my <= m_maxpos.y)
		return true;

	return false;
}

bool Text::IsClicked(int button)
{
	if (!m_useable) return false;
	return IsHovered() && UI::GetMousebutton(button);
}