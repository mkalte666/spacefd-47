/*
File:masterutil.cpp
Purpose: Contains the main utility funcitons and main engine functions
Author(s): Malte Kie�ling (mkalte666)
*/
#include "base.h"

#include "glutil.h"
#include "setting.h"
#include "SceneManager.h"
#include "window.h"
#include "menu.h"
#include "light.h"
#include "time.h"
#include "Animation.h"
#include "Renderer.h"
#include "UI.h"
#include "Sound.h"
#include "Interaction.h"

//Static vars
bool Engine::m_init = false;
std::map<std::string, GenericCallback> *Engine::m_callbacks = NULL;
std::map<std::string, void*> *Engine::m_callbackArgs = NULL;
FT_Library Engine::m_ftLibary = NULL;


/*
* @name: Engine::INit
* @param: void
* @return: void
* @description:  
INITIALISATION OF EVERYTHING
Loads all libarys and sets up window. After That it Starts the "GlobalLoop". 
Must be Called before anything else. Really. Call it in the main. 
*/
bool Engine::Init()
{
	setting<int>width("width");
	setting<int>height("height");
	setting<std::string>title("title");

	Renderer::Init(width.GetSettingFast(), height.GetSettingFast(), title.GetSettingFast()); //Init renderer
	MessageManager::Init(); //Init message system
	SceneManager::Init(); //Init scene Manager

	//Start FreeType libary
	if (FT_Init_FreeType(&m_ftLibary)) {
		std::cout << "FATAL: Could not Init FreeType libary.";
		return false;
	}

	//start FFMPEG
	av_register_all();
	avcodec_register_all();

	//Start sound system
	Sound::Init();
	Loop();
	return true;
}


/*
* @name: Engine::Loop
* @param: void
* @return: void
* @description: The Master-loop (Wooo!). It updates all Libary-stuff and then Updates the Scene-Manager.
* @TODO: add a draw-order. here or somewhere else. BUT DRAW-ORDER GOD DAMMIT
*/
void Engine::Loop()
{
	Startup();

	while(true) {
		UI::Update();
		MessageManager::Update();
		SceneManager::Update();
		Renderer::Render();
	}
	
	Cleanup();
}

/*
* @name: Engine::RegisterCallback
* @return: void
* @param:	GenericCallback callback: function that will be added
			std::string ident: string with the call-name for the function
			void* arg: argument that will be passed to the Callback when called
* @description: Addes a callback to the callback-system
* @notes:
*/
void Engine::RegisterCallback(GenericCallback callback, std::string ident, void* arg1)
{
	if(m_callbacks == NULL) {
		m_callbacks = new std::map<std::string, GenericCallback>();
		m_callbackArgs = new std::map<std::string, void*>();
	}
	if(m_callbacks->find(ident)!=m_callbacks->end()) return;

	m_callbacks->insert(std::pair<std::string, GenericCallback>(ident, callback));
	m_callbackArgs->insert(std::pair<std::string, void*>(ident, arg1));
}

/*
* @name: Engine::RunCallback
* @return: void
* @param:	std::string ident: identification for the callback that will be called
			void* pArg: argument that will be passed to the called callback
* @description: Calls a callback previously registerd
* @notes:
*/
void Engine::RunCallback(std::string ident, void* pArg)
{
	if(m_callbacks->find(ident)==m_callbacks->end()) {
		std::cout << "WARNING: Tried to call not exsisting callback '" << ident << "'!" << std::endl;	
		return;
	}
	std::map<std::string, GenericCallback>::iterator pos = m_callbacks->find(std::string(ident));
	std::map<std::string, void*>::iterator argpos = m_callbackArgs->find(std::string(ident));

	pos->second(argpos->second, pArg);
}

/*
* @name: Engine::RemoveCallback
* @return: void
* @param:	std::string ident: name of the callback that will be removed
* @description: Removes a previously registerd callback
* @notes:
*/
void Engine::RemoveCallback(std::string ident)
{
	if (m_callbacks == NULL) {
		m_callbacks = new std::map<std::string, GenericCallback>();
		m_callbackArgs = new std::map<std::string, void*>();
		std::cout << "WARNING: Tried to remove not exsisting callback '" << ident << "'!" << std::endl;
		return;		//Return. If we werent initialised yet, there is nothing no remove
	}
	if(m_callbacks->find(ident)==m_callbacks->end()) {
		std::cout << "WARNING: Tried to remove not exsisting callback '" << ident << "'!" << std::endl;	
		return;
	}

	std::map<std::string, GenericCallback>::iterator pos = m_callbacks->find(std::string(ident));
	std::map<std::string, void*>::iterator argpos = m_callbackArgs->find(std::string(ident));
	m_callbacks->erase(pos);
	m_callbackArgs->erase(argpos);
}

/*
* @name: Engine::Startup
* @description: Calls the spawnEngine-callback
* @notes:
*/
void Engine::Startup()
{
	RunCallback("spawnEngine");
}

/*
* @name: Engine::Cleanup
* @return: void
* @description: Destory some systems
* @notes:
*/
void Engine::Cleanup()
{
	Sound::Destroy();
}

Callback::Callback(GenericCallback callback, const char* ident, void* arg1)
{
	Engine::RegisterCallback(callback, ident, arg1);
	m_ident = ident;
}

Callback::~Callback() 
{

}

void Callback::Run()
{
	Engine::RunCallback(m_ident.c_str());
}


//FONT SYSTEM

FT_Library Engine::GetFontSystem()
{
	return m_ftLibary;
}

