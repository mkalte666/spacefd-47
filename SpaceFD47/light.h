/*
File: light.h
Purpose: Contains the light class
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once
#include "base.h"
#include "BaseClass.h"
#include "masterutil.h"
#include "Renderer.h"

/* 
* @name: light
* @parent: BaseClass
* @description: Effect that creates a lightsource on a position with a radius and a strenght and a color
*/
class light :
	public BaseClass
{
public:
	light(std::string program);
	virtual ~light(void);
	
	virtual void Update(float time);
	virtual void Draw(GlProgram&program);

	virtual void SetPosition(glm::vec2 pos);
	virtual void GetPosition(glm::vec2&pos);
	virtual void Move(glm::vec2 dir);

	virtual void SetColor(glm::vec3 color);
	virtual void GetColor(glm::vec3&color);
	
	virtual void SetStrength(float strength);
	virtual float GetStrength();

	virtual void SetRadius(float radius);
	virtual float GetRadius();
private:
	glm::vec2				m_pos;
	glm::vec3				m_color;
	std::auto_ptr<Effect>	m_effect;
	float					m_strength;
	float					m_radius;
};

