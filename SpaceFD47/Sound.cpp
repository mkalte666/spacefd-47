#include "Sound.h"
#include "alutil.h"
#include "setting.h"
//Static vars
ALCdevice *Sound::m_device = NULL;
ALCcontext *Sound::m_context = NULL;
std::list<SoundSource> Sound::m_sources = std::list<SoundSource>();
std::map<std::string, ALuint> Sound::m_sounds = std::map<std::string, ALuint>();
unsigned long	Sound::m_maxSouceId = 0;

//invalid return
SoundSource INVALID_RETURN;

Sound::Sound()
{
}


Sound::~Sound()
{
}

bool Sound::Init()
{
	m_device = alcOpenDevice(NULL);
	if (!m_device) {
		std::cout << "ERROR: Could not open AL device. " << std::endl;
		return false;
	}

	m_context = alcCreateContext(m_device, NULL);
	if (m_context == NULL || alcMakeContextCurrent(m_context) == ALC_FALSE)
	{
		if (m_context != NULL)
			alcDestroyContext(m_context);
		alcCloseDevice(m_device);
		std::cout << "ERROR: Could not set Context!" << std::endl;
		return false;
	}

	std::cout << "INFO: Sound Init: Opened \"" << alcGetString(m_device, ALC_DEVICE_SPECIFIER) << "\"" << std::endl;

	//Precache sounds to avoid loading
	setting<std::string> soundcache("soundcache");
	setting<std::string> soundpath("soundpath");
	setting<std::string> soudsSetting("sounds", soundcache.GetSettingFast());
	std::vector<std::string> sounds = soudsSetting.GetSettingArray();
	for (auto i = sounds.begin(); i != sounds.end(); i++) {
		setting<std::string> soundfilepath(*i, soundcache.GetSettingFast());
		std::string filename = soundpath.GetSetting() + soundfilepath.GetSettingFast();
		ALuint bufferid = GenerateBufferFromFile(filename);
		m_sounds.insert(make_pair(*i, bufferid));
	}

	return true;
}

void Sound::Destroy()
{
	if (m_context == NULL || m_device == NULL)
		return;

	for (auto i = m_sources.begin(); i != m_sources.end(); i++) {
		i->Cleanup();
	}

	for (auto i = m_sounds.begin(); i != m_sounds.end(); i++) {
		alDeleteBuffers(1, &i->second);
	}
	m_sources.clear();

	alcMakeContextCurrent(NULL);
	alcDestroyContext(m_context);
	alcCloseDevice(m_device);

}

void Sound::SetListenerVel(glm::vec3 v)
{
	alListenerfv(AL_VELOCITY, &v[0]);
}

void Sound::SetListenerPos(glm::vec3 pos)
{
	alListenerfv(AL_POSITION, &pos[0]);
	float orientation[] = { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 };
	alListenerfv(AL_ORIENTATION, orientation);
}

SoundSourceIdent Sound::AddSource(SoundSource s)
{
	m_maxSouceId++;
	s.id = m_maxSouceId;
	m_sources.push_back(s);
	return s.id;
}

SoundSource&	Sound::GetSource(SoundSourceIdent ident)
{
	for (auto i = m_sources.begin(); i != m_sources.end(); i++) {
		if (i->id == ident) {
			return *i;
			break;
		}
	}
	
	return INVALID_RETURN;
}

void Sound::DelSource(SoundSourceIdent ident)
{
	for (auto i = m_sources.begin(); i != m_sources.end(); i++) {
		if (i->id == ident) {
			i->Cleanup();
			i = m_sources.erase(i);
			break;
		}
	}
}

ALuint Sound::GetSoundBuffer(std::string sound)
{
	ALuint result = 0;
	auto i = m_sounds.find(sound);
	if (i != m_sounds.end())
		result = i->second;
	return result;
}

void SoundSource::Load(std::string name)
{
	if (source == 0) {
		alGenSources(1, &source);
	}
	buffer = Sound::GetSoundBuffer(name);

}
void SoundSource::Play()
{
	if (paused)
	{
		alSourcei(source, AL_BUFFER, buffer);                                    //Link the buffer to the source
		alSourcef(source, AL_PITCH, pitch);                                 //Set the pitch of the source
		alSourcef(source, AL_GAIN, gain);                                 //Set the gain of the source
		alSourcefv(source, AL_POSITION, &pos[0]);                                 //Set the position of the source
		alSourcefv(source, AL_VELOCITY, &vel[0]);                                 //Set the velocity of the source
		alSourcei(source, AL_LOOPING, looping);
	}
	alSourcePlay(source);
	paused = false;
}

void SoundSource::Cleanup()
{
	buffer = 0;
	if (source != 0)
		alDeleteSources(1, &source);
}