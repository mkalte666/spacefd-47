/*
File: time.cpp
Purpose: contains the functions for time.h
Author(s): Malte Kie�ling (mkalte666)
*/

#include "time.h"

/*
* @name: time
* @param: void
* @return: void
* @description: constructor for the time-class. Resets the glfw-time and fills everything with the initial values.
*/
TimeManager::TimeManager(void)
{
	glfwSetTime(0.0f);
	m_time = (float)glfwGetTime();
	m_timeElapsed = 0;
}

/*
* @name: ~time
* @param: void
* @return: void
* @description: destructor for the time-class. Clears the callback-container.
*/
TimeManager::~TimeManager(void)
{
	glfwSetTime(0);
	m_timers.clear();
}

/*
* @name: Update
* @param: void
* @return: void
* @description: Updates global time, updates times and ggf. calles timer callbacks
*/
void TimeManager::Update(void)
{
	m_timeElapsed = (float)glfwGetTime() - m_time;
	m_time = (float)glfwGetTime();

	//We update our timers. If we have timers...
	if(m_timers.size()>0) {
		for (auto i = m_timers.begin(); i != m_timers.end(); i++) {
			//And there is a timer on that adress
			if(*i) {
				//Lets test if its running
				if((*i)->m_running) {
					//If yes, we refresh the times. 
					(*i)->m_curtime += m_timeElapsed;
					(*i)->m_timeelapsed = m_timeElapsed;
					//Oh, and if it have an endtime (timer::m_endtime >0) then we want to stop it
					if ((*i)->m_endtime >0 && (*i)->m_curtime >= (*i)->m_endtime) {
						(*i)->m_running = false;
						//Oh, and if it has a callback, we want to call it
						if ((*i)->m_hasCallback)
							(*i)->callback(m_timeElapsed, (*i)->m_callbackArg);
					}
				} 
				//If the timer is not running, to time elapsed. so set it to null!
				else {
					(*i)->m_timeelapsed = 0.0f;
				}
			}
		}
	}	
}


/*
* @name: RegisterTimer
* @param:	bool started=true: gets the timer started when it's added?
* @return: TimerIdent
* @description: Creates a new timer and returns and TimerIdent to acces it.
*/
TimerIdent TimeManager::RegisterTimer(bool started)
{
	ManagedTimer* newtimer = new ManagedTimer;
	newtimer->m_hasCallback = false;
	newtimer->m_initoffset = m_time;
	newtimer->m_endtime = 0.0f;
	newtimer->m_curtime = 0.0f;
	newtimer->m_timeelapsed = 0.0f;
	newtimer->m_callbackArg = NULL;
	newtimer->callback = NULL;
	newtimer->m_running = started;
	m_timers.push_back(newtimer);
	return newtimer;
}

/*
* @name: RegisterTimer
* @param:	float timeToEnd: time after the timer will end
			bool started=true: gets the timer started when it's added?
* @return: TimerIdent
* @description: Creates a new timer and returns and TimerIdent to acces it.
*/
TimerIdent TimeManager::RegisterTimer(float timeToEnd, bool started)
{
	ManagedTimer* newtimer = new ManagedTimer;
	newtimer->m_hasCallback = false;
	newtimer->m_initoffset = m_time;
	newtimer->m_endtime = timeToEnd;
	newtimer->m_curtime = 0.0f;
	newtimer->m_timeelapsed = 0.0f;
	newtimer->m_callbackArg = NULL;
	newtimer->callback = NULL;
	newtimer->m_running = started;
	m_timers.push_back(newtimer);
	return newtimer;
}

/*
* @name: RegisterTimer
* @param:	TimerCallback callback: callback that will be called when the timer ends.
			float timeToEnd: time after the timer will end
			bool started=true: gets the timer started when it's added?
* @return: TimerIdent
* @description: Creates a new timer and returns and TimerIdent to acces it.
*/
TimerIdent TimeManager::RegisterTimer(TimerCallback callback, float timeToCall, bool started)
{
	ManagedTimer* newtimer = new ManagedTimer;
	newtimer->m_hasCallback = true;
	newtimer->m_initoffset = m_time;
	newtimer->m_endtime = timeToCall;
	newtimer->m_curtime = 0.0f;
	newtimer->m_timeelapsed = 0.0f;
	newtimer->m_callbackArg = NULL;
	newtimer->callback = callback;
	newtimer->m_running = started;
	m_timers.push_back(newtimer);
	return newtimer;
}

/*
* @name: RegisterTimer
* @param:	TimerCallback callback: callback that will be called when the timer ends.
			float timeToEnd: time after the timer will end
			void* arg: argument for the timer-callback
			bool started=true: gets the timer started when it's added?
* @return: TimerIdent
* @description: Creates a new timer and returns and TimerIdent to acces it.
*/
TimerIdent TimeManager::RegisterTimer(TimerCallback callback, float timeToCall, void* arg, bool started)
{
	ManagedTimer* newtimer = new ManagedTimer;
	newtimer->m_hasCallback = true;
	newtimer->m_initoffset = m_time;
	newtimer->m_endtime = timeToCall;
	newtimer->m_curtime = 0.0f;
	newtimer->m_timeelapsed = 0.0f;
	newtimer->m_callbackArg = arg;
	newtimer->callback = callback;
	newtimer->m_running = started;
	m_timers.push_back(newtimer);
	return newtimer;
}

//TODO: MAKE THE m_timer[n] - MEMBER AVAILBE FOR NEW TIMERS



/*
* @name: RemoveTimer
* @param: timer* ident: Timer to remove
* @return: void
* @description: Removes timer by TimerIdent
*/
void TimeManager::RemoveTimer(TimerIdent& ident)
{
	for (auto i = m_timers.begin(); i != m_timers.end(); i++) {
		if(ident == (*i)) {
			m_timers.erase(i);
			i = m_timers.begin();
		}
	}
	ident = NULL;
}

Timer::Timer()
{
	m_time_started = glfwGetTime();
}

Timer::~Timer()
{

}


double Timer::Get()
{
	return glfwGetTime() - m_time_started;
}

void Timer::Reset(double time)
{
	m_time_started = glfwGetTime() + time;
}
