﻿/*
File: SpaceFD47.cpp
Purpose: Startpoint of the application
Author(s): Malte Kießling (mkalte666)
*/
#include "base.h"

#include "masterutil.h"


int main(int argc, char* argv[])
{
#ifndef _DEBUG
#ifdef _WIN32
	//FreeConsole();
#endif
#endif
	Engine::Init();

	//(╯°□°)╯︵┻━┻;
}

