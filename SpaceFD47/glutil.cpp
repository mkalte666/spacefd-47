/*
File: glutil.cpp
Purpose: Contains some Utility functions for OpenGL 
Author(s): Malte Kie�ling (mkalte666)
*/
#include "base.h"

/*
* @name: LoadProgram
* @return: GLuint that represents the Program on the GPU
* @param:	std::string vertex_file_path: path to a vertexshader-file
			std::string fragmet_file_path: path to a fragmentshader-file
* @description: Compiles a Program from a vertex- and a fragmentshader
* @notes:
*/
GLuint LoadProgram(std::string vertex_file_path, std::string fragment_file_path){

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if(VertexShaderStream.is_open())
    {
        std::string Line = "";
        while(getline(VertexShaderStream, Line))
            VertexShaderCode += "\n" + Line;
        VertexShaderStream.close();
	}
	else {
		return GL_INVALID_INDEX;
	}

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if(FragmentShaderStream.is_open()){
        std::string Line = "";
        while(getline(FragmentShaderStream, Line))
            FragmentShaderCode += "\n" + Line;
        FragmentShaderStream.close();
    }
	else {
		return GL_INVALID_INDEX;
	}

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> VertexShaderErrorMessage(InfoLogLength);
    glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
    fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);

    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> FragmentShaderErrorMessage(InfoLogLength);
    glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
    fprintf(stdout, "%s\n", &FragmentShaderErrorMessage[0]);

    // Link the program
    fprintf(stdout, "Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> ProgramErrorMessage( std::max(InfoLogLength, int(1)) );
    glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
    fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return ProgramID;
}

/*
* @name: GenereteFramebuffer
* @return: void
* @param:	int w: width of the framebuffer
			int h: height of the framebuffer
			GLuint &dstFboId: GLuint to write the ID of the framebuffer to
			GLuibt &dstTex: GLuint to write the ID of the Texture to
			GLuint &dstDepth: GLuint to write the ID of the Depthbuffer to
* @description: Creates an RGBA framebuffer(aka rendertarget) on the gpu
* @notes:
*/
void GenerateFramebuffer(int w, int h, GLuint &dstFboId, GLuint &dstTex, GLuint &dstDepth)
{
	GLuint FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	GLuint renderedTexture;
	glGenTextures(1, &renderedTexture);
 
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, renderedTexture);
 
	// Give an empty image to OpenGL ( the last "0" )
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, w, h, 0,GL_RGBA, GL_UNSIGNED_BYTE, 0);
 
	// Poor filtering. Needed !
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// The depth buffer
	GLuint depthrenderbuffer;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, w, h);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	// Set "renderedTexture" as our colour attachement #0
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);
 
	// Set the list of draw buffers.
	GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) std::cout <<"Error while Creating Framebuffer!";
	else {
		dstFboId = FramebufferName;
		dstTex = renderedTexture;
		dstDepth = depthrenderbuffer;
	}
}

/*
* @name: SetNewTextureData
* @return: void
* @param:	GLuint textureID: ID of the texture that will be modified
			char *data: image-data in raw pixel format
			int widht: widht of the image in data
			int height: height of the image in data
			GLint format: format of the data in data
			GLint type: type of the texture data on the gpu
			bool filter: if true, texture will have mini-maps and GL_LINEAR filters
* @description: Sets new Texture-information to a GPUTexture. 
* @notes: In case data=null, an empty texture is created
*/
void SetNewTextureData(GLuint textureID, char * data, int width, int height, GLint format, GLenum type, bool filter)
{
	glBindTexture(GL_TEXTURE_2D, textureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// Give the image to OpenGL
	if (data != NULL) {
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, type, GL_UNSIGNED_BYTE, data);
	}
	else {
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, type, GL_UNSIGNED_BYTE, 0);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	//Filtering mode. Sometinmes you want filtering (often!), but sometimes not so much (for letters etc.)
	if (filter) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		//glGenerateMipmap(GL_TEXTURE_2D);
	}
	

	int errorcode = glGetError();
	if (errorcode != GL_NO_ERROR) {
		std::cout << "Oh, something went wrong (In Texture creation): ";
		std::cout << std::hex << errorcode;
		std::cout << "\n";
	}
}

//Stolen and modified from http://stackoverflow.com/questions/22930109/call-to-avformat-find-stream-info-prevents-decoding-of-simple-png-image
//Helper for BufferFromImage
AVCodecContext* openContext(AVMediaType mediaType, AVFormatContext* formatContext)
{
	auto ret = 0;
	if ((ret = av_find_best_stream(formatContext, mediaType, -1, -1, nullptr, 0)) < 0)
	{
		std::cout << "Failed to find video stream." << std::endl;
		return nullptr;
	}

	auto codecContext = formatContext->streams[ret]->codec;
	auto codec = avcodec_find_decoder(codecContext->codec_id);
	if (!codec)
	{
		std::cout << "Failed to find codec." << std::endl;
		return nullptr;
	}

	codecContext->thread_count = 1;
	if ((ret = avcodec_open2(codecContext, codec, nullptr)) != 0)
	{
		std::cout << "Failed to open codec context." << std::endl;
		return nullptr;
	}

	return codecContext;
}

/*
* @name: BufferFromImage
* @return: unsigned int with the length of the texture-data
* @param:	std::string path: filename of the image that will be read
			unsigned char**dst: pointer to a uchar* that will contain the pixeldata
			int &w: reference to a int that will contain the width of the read image
			int &h: reference to a int that will contain the height of the read image
* @description: Reads an image with the help of FFMPEG (libav) and creates a pixelbuffer
* @notes: All data are int the GL_RGBA-format
*/
unsigned int BufferFromImage(std::string path, unsigned char **dst, int &w, int &h)
{
	unsigned int ret = 0;
	auto formatContext = avformat_alloc_context();
	if ((ret = avformat_open_input(&formatContext, path.c_str(), NULL, NULL)) != 0)
	{
		std::cout << "ERROR: Failed to open '" << path << "'!" << std::endl;
		return -1;
	}

	auto codecContext = openContext(AVMEDIA_TYPE_VIDEO, formatContext);
	
	AVPacket packet;
	av_init_packet(&packet);

	if ((ret = av_read_frame(formatContext, &packet)) < 0) {
		std::cout << "ERROR: Failed to read frame from '" << path << "'!" << std::endl;
		return -1;
	}

	auto frame = av_frame_alloc();
	auto decode_ok = 0;
	if ((ret = avcodec_decode_video2(codecContext, frame, &decode_ok, &packet)) < 0 || !decode_ok) {
		std::cout << "ERROR: Failed to decode frame from '" << path << "'!" << std::endl;
		return -1;
	}

	PixelFormat destFormat = PIX_FMT_RGBA;
	AVPicture destPicture;
	avpicture_alloc(&destPicture, destFormat, frame->width, frame->height);
	SwsContext *ctxt = sws_getContext(frame->width, frame->height, (PixelFormat)frame->format, frame->width, frame->height, destFormat, SWS_BILINEAR, nullptr, nullptr, nullptr);
	sws_scale(ctxt, frame->data, frame->linesize, 0, frame->height, destPicture.data, destPicture.linesize);
	sws_freeContext(ctxt);
	*dst = new unsigned char[frame->width*frame->height * 4];
	avpicture_layout(&destPicture, destFormat, frame->width, frame->height, *dst, frame->width*frame->height * 4);
	w = frame->width;
	h = frame->height;


	av_frame_free(&frame);
	av_free_packet(&packet);

	avcodec_close(codecContext);

	avformat_close_input(&formatContext);
	av_free(formatContext);
	
	return w*h * 4;
}
