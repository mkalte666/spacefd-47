#pragma once
#include "BasePlayer.h"
#include "Interaction.h"

#define NPC_FIGHTER_NAME std::string("npcfighter")
class NPCFighter :
	public BasePlayer
{
public:
	NPCFighter(std::string name);
	~NPCFighter();

	virtual void Update(float timeElapsed);
	virtual void PrimaryAttack();
private:
	PhysicsIdent m_physical;
	glm::vec2	m_weaponOffset;
	glm::vec2	m_shootDir;

	TimerIdent	m_testTimer;
};

extern BaseClass* MapCreator_NPCFighter(std::vector<std::string> arguments);