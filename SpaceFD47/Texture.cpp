#include "Texture.h"
#include "setting.h"
#include "glutil.h"

Texture::Texture()
{
	m_textureId = TEXTURE_INVALID_ID;
	m_data = NULL;
}

Texture::Texture(std::string name) 
	: m_name(name), m_textureId(0), m_width(0), m_height(0)
{
	m_textureId = TEXTURE_INVALID_ID;
	setting<std::string> texturedir("texturedir");
	m_filename = texturedir.GetSettingFast() + name;
	m_alpha = 1.0;
	std::cout << "Reading Texture: " << m_filename << std::endl;
	
	m_type = GL_RGBA;
	int result = BufferFromImage(m_filename, &m_data, m_width, m_height);
	if (result == -1) {
		std::cout << "ERROR: Invalid texture '" << m_filename << "'!" << std::endl;
	}
}

Texture::Texture(unsigned char* data, int w, int h, GLuint type)
{
	m_textureId = TEXTURE_INVALID_ID;
	m_width = w;
	m_height = h;
	m_type = type;
	m_data = (unsigned char*) data;
	
}

Texture::~Texture(void)
{
	glDeleteTextures(1, &m_textureId);
	m_filename.clear();
}

void Texture::Create()
{
		if (m_textureId == TEXTURE_INVALID_ID) {
			glGenTextures(1, &m_textureId);
		}	

		SetNewTextureData(m_textureId, (char*)m_data, m_width, m_height, GL_RGBA, m_type);
}


void Texture::Reload(unsigned char* data, int w, int h, GLint format, GLenum type)
{
	
	
	m_width = w;
	m_height = h;
	m_type = type;
	/*if (m_data!=NULL)
		delete m_data;*/

	//Copy texture to local storage
	m_data = data;
	//memcpy_s(m_data, size, data, size);
	SetNewTextureData(m_textureId, (char*)data, w, h, GL_RGBA, type);
	//Create();
}


GLuint Texture::GetTextureId()
{
	return m_textureId;
}

std::string& Texture::GetFilename()
{
	return m_filename;
}

std::string& Texture::GetName()
{
	return m_name;
}

void Texture::GetDimensions(glm::vec2 &dim)
{
	dim = glm::vec2(m_width, m_height);
}


float Texture::GetAlpha()
{
	return m_alpha;
}

void Texture::SetAlpha(float alpha)
{
	m_alpha = alpha;
}

LowFilterTexture::LowFilterTexture()
: Texture()
{

}

LowFilterTexture::LowFilterTexture(const char* name)
: Texture(name)
{

}
LowFilterTexture::LowFilterTexture(unsigned char* data, int w, int h, GLuint type)
: Texture(data, w, h, type)
{

}

LowFilterTexture::~LowFilterTexture()

{
	Texture::~Texture();
}

void LowFilterTexture::Create()
{
	if (m_textureId == TEXTURE_INVALID_ID) {
		glGenTextures(1, &m_textureId);
	}

	SetNewTextureData(m_textureId, (char*)m_data, m_width, m_height, GL_RGBA, m_type, false);
}