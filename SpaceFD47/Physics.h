/*
File: Physics.h
Purpose: Header for the physics-class. Container-class that handles Physic. (Captain obvious strikes again)
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once
#include "BaseClass.h"
#include "World.h"
#include "collision.h"
#include "Interaction.h"

struct PhysicalObj
{
	glm::vec2	v;
	glm::vec2	a;
	float		mass;
	BaseClass*	obj;
	std::auto_ptr<CollisionPolygon> collider;
	uint32_t	collision_type;
	int			skipTicks = 0;
};

typedef PhysicalObj* PhysicsIdent;

class Physics :
	public BaseClass
{
public:
	Physics(void);
	~Physics(void);

	void	SetWorld(World&world);
	void	GetWorld(World&world);

	PhysicsIdent AddObj(CollisionPolygon *collider, BaseClass* obj, uint32_t type = TYPE_ALL);
	bool	RemoveObj(BaseClass* obj);
	
	void	ActualUpdate(float TimeElapsed);

	static Physics& GetActiveSystem();
	
	void	SetAsActiveSystem();

	
private:
	World	m_world;
	std::vector<PhysicalObj*> m_objects;
	static Physics* m_active;
	std::recursive_mutex	m_mutex;
	std::thread		m_updateThread;
};

