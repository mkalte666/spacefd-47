#pragma once
#include "masterutil.h"
#include "BaseClass.h"
#include "maptypes.h"

class Map;

struct mapblock
{
	std::string name;
	std::vector<std::string> raw_content;

	bool Execute(Map* parent);
	
};

class Map : public BaseClass
{
public:
	Map(std::string name);
	virtual ~Map();

	std::map<std::string, float> & NumVars();
	std::map<std::string, std::string> & StringVars();
	std::vector<MapObject>& Objects();
	std::map < std::string, mapblock>& Blocks();
	virtual void Update(float timeElapsed);
private:
	std::map<std::string, float>		m_numeric_vars;
	std::map<std::string, std::string>	m_string_vars;
	std::map < std::string, mapblock>	m_blocks;
	std::vector<MapObject>				m_objects;

	bool			m_running;
	std::string		m_name;
};

