#pragma once

#include "base.h"

#define TEXTURE_INVALID_ID 65536


class Texture;
typedef Texture* TextureIdent;

class Texture
{
public:

	Texture();
	Texture(std::string name);
	Texture(unsigned char* data, int w, int h, GLuint type);
	virtual ~Texture(void);

	virtual void Create();
	virtual void Reload(unsigned char* data, int w, int h, GLint format, GLenum type);

	virtual GLuint GetTextureId();

	virtual std::string& GetFilename();
	virtual void		GetDimensions(glm::vec2 &dim);
	
	virtual void SetAlpha(float alpha);
	virtual float GetAlpha();

	virtual std::string& GetName();
protected:
	std::string m_filename;
	std::string m_name;
	GLuint m_textureId;
	int m_width;
	int m_height;
	float m_alpha;
	unsigned char*	m_data;
	GLuint			m_type;
};

class LowFilterTexture
	: public Texture
{

public:
	LowFilterTexture();
	LowFilterTexture(const char* name);
	LowFilterTexture(unsigned char* data, int w, int h, GLuint type);
	~LowFilterTexture(void);

	virtual void Create();
};