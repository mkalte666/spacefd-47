/*
File: light.cpp
Purpose: File that contains functions for the light-class
Author(s): Malte Kie�ling (mkalte666)
*/
#include "light.h"

/*
* @name: LightEffectDrawCallback
* @param:	shader:	GLuint with the shader-id
			arg:	pointer to a light-class
* @return:	void
* @description: Class the Draw-outine of the given ligth-class
*/
static void LightEffectDrawCallback(GlProgram&program, void* arg)
{
	((light*)arg)->Draw(program);
}

/*
* @name: light::light
* @param:	shader: const char* with the name of the shader that will be used
* @return: void
* @description: Creates a basic light
*/
light::light(std::string program)
{
	m_effect.reset(new Effect(program));
	m_effect->AddDrawCallback(LightEffectDrawCallback, this);
	Renderer::AddEffectEnd(m_effect.get());
	m_pos = glm::vec2(0,0);
	m_color = glm::vec3(0,0,0);
	m_strength=0.0;
	m_radius = 0.0;
}

/*
* @name: light::~light
* @param:	void
* @return: void
* @description: Delets the light
*/
light::~light(void)
{
	Renderer::RemoveEffect(m_effect.get());
	m_effect.release();
	BaseClass::~BaseClass();
}

/*
* @name: light::Draw
* @param:	shader: GLuint with the shader that is used in it
* @return:	void
* @description: Gives additional Parameters the the shader so the effect does look as it shut
*/
void light::Draw(GlProgram&program)
{
	GLuint posID = program["position"];
	GLuint strID = program["strength"];
	GLuint clrID = program["incolor"];
	GLuint radID = program["radius"];

	glUniform1f(strID, m_strength);
	glUniform1f(radID, m_radius);
	glUniform2f(posID, m_pos.x, m_pos.y);
	glUniform3f(clrID, m_color.r, m_color.g, m_color.b);
}

/*
* @name: light::Update
* @param:	time: float with the time in seconds since the last call
* @return: void
* @description: ~
*/
void light::Update(float time)
{
	
}

/*
* @name: light::SetPosition
* @param:	pos: glm::vec2 that contains the position of the light
* @return: void
* @description: Sets the light to a new position
*/
void light::SetPosition(glm::vec2 pos)
{
	m_pos = pos;
}

/*
* @name: light::GetPosition
* @param:	pos: &glm::vec2 that will contain the pos of the light
* @return: void
* @description: Sets the pos to m_pos (so pos contains the position of the light)
*/
void light::GetPosition(glm::vec2&pos)
{
	pos = m_pos;
}

/*
* @name: light::Move
* @param:	pos: glm::vec2 that contains the direction
* @return: void
* @description: Moves the light in a direction
*/
void light::Move(glm::vec2 dir)
{
	m_pos+=dir;
}

/*
* @name: light::SetColor
* @param:	color: glm:vec3 with the RGB (0..1) color
* @return: void
* @description: Sets the color of the light
*/
void light::SetColor(glm::vec3 color)
{
	m_color = color;
}

/*
* @name: light::GetColor
* @param:	color: &glm::vec3 that will contain the color of the light
* @return: void
* @description:	sets color to the color of the light
*/
void light::GetColor(glm::vec3&color)
{
	color = m_color;
}

/*
* @name: light::SetRadius
* @param:	radius: float with the radius of the light
* @return: void
* @description: Sets the radius of the light
*/
void light::SetRadius(float radius)
{
	m_radius = radius;
}

/*
* @name: light::GetRadius
* @param:	void
* @return:	float: the radius of the light
* @description: Returns the Radius of the light
*/
float light::GetRadius()
{
	return m_radius;
}

/*
* @name: light::SetStrength
* @param:	strenght: float with the strength of the light
* @return:	void
* @description: Sets the strenght of the light
*/
void light::SetStrength(float strength)
{
	m_strength = strength;
}

/*
* @name: light::GetStrength
* @param:	void
* @return:	float: the strenght of the light
* @description: Returns the strength of the light
*/
float light::GetStrength()
{
	return m_strength;
}
