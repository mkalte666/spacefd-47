/*
File: collision.cpp
Purpose: Contains Functions for collision detection
Author(s): Malte Kie�ling (mkalte666)
*/
#include "collision.h"
#include "Interaction.h"

LineSegment::LineSegment(void)
: m_start(0), m_end(0)
{

}

LineSegment::LineSegment(glm::vec2&start, glm::vec2&end)
	: m_start(start), m_end(end)
{

}

LineSegment::~LineSegment(void)
{

}

bool LineSegment::Collision(LineSegment&segment)

{
	//Code modified from http://www.spieleprogrammierer.de/wiki/2D-Kollisionserkennung
	const float denom = (segment.m_end.y - segment.m_start.y) * (m_end.x - m_start.x) -
                        (segment.m_end.x - segment.m_start.x) * (m_end.y - m_start.y);
    if (abs(denom) < EPSILON) return false;
    
    const float ua = ((segment.m_end.x - segment.m_start.x) * (m_start.y - segment.m_start.y) -
                      (segment.m_end.y - segment.m_start.y) * (m_start.x - segment.m_start.x)) / denom;
    const float ub = ((m_end.x - m_start.x) * (m_start.y - segment.m_start.y) -
                      (m_end.y - m_start.y) * (m_start.x - segment.m_start.x)) / denom;
    return ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1;

}

//------------------CollisionPolygon class-----------//
CollisionPolygon::CollisionPolygon( BaseClass *parent)
	: m_segments(0), m_parent(parent), m_position(0)
{

}

CollisionPolygon::CollisionPolygon(std::vector<LineSegment> parts, BaseClass *parent)
	: m_parent(parent)
{
	//If we have a parent, we have to pos us and scale the segments to screenspace
	m_position = glm::vec2(0);
	if(m_parent) {
		glm::vec2 pos;
		glm::vec2 scale;
		m_parent->GetPosition(pos);
		m_parent->GetSize(scale);
		for( unsigned int i = 0; i < parts.size(); i++) {
			parts[i].m_start.x*=scale.x;
			parts[i].m_start.y*=scale.y;
			parts[i].m_end.x*=scale.x;
			parts[i].m_end.y*=scale.y;
			parts[i].m_start += pos;
			parts[i].m_end += pos;
		}
		m_position = pos;
	}
	m_segments = parts;
	 
}

CollisionPolygon::CollisionPolygon(setting<float>&setting, BaseClass *parent)
	: m_parent(parent)
{
	std::vector<float> rawPositions = setting.GetSettingArray();
	if(rawPositions.size() % 4 != 0) {
		std::cout << "WARNING: Trying to create CollisionPolygon from invalid setting!";
		return;
	}

	std::vector<LineSegment> parts;
	for(unsigned int i = 0; i < rawPositions.size(); i+=4) {
		LineSegment segment;
		segment.m_start = glm::vec2(rawPositions[i], rawPositions[i+1]);
		segment.m_end = glm::vec2(rawPositions[i+2], rawPositions[i+3]);
		parts.push_back(segment);
	}

	//If we have a parent, we have to pos us and scale the segments to screenspace
	if(m_parent) {
		glm::vec2 pos;
		glm::vec2 scale;
		m_parent->GetPosition(pos);
		m_parent->GetSize(scale);
		for(unsigned int i = 0; i < parts.size(); i++) {
			parts[i].m_start.x*=scale.x;
			parts[i].m_start.y*=scale.y;
			parts[i].m_end.x*=scale.x;
			parts[i].m_end.y*=scale.y;
			parts[i].m_start += pos;
			parts[i].m_end += pos;
		}
		m_position = pos;
	}
	m_segments = parts;

}

CollisionPolygon::~CollisionPolygon()
{

}

void CollisionPolygon::Update()
{
	if (m_parent) {
		glm::vec2 pos;
		m_parent->GetPosition(pos);
		if (m_position != pos) {
			SetPosition(pos);

		}
	}
}

BaseClass* CollisionPolygon::GetParent()
{
	return m_parent;
}

bool CollisionPolygon::Collide(CollisionPolygon& polygon)
{
	//Update me and my friend
	Update();
	polygon.Update();
	//Pure collision detection is alot to much inacurate.
	//If the polygon has a parent and the polygon to collide with too, we first compare the raw boxes (pos / pos+size)
	if (m_parent != NULL) {
		BaseClass* collider_parent;
		if ((collider_parent = polygon.GetParent()) != NULL) {
			glm::vec2 pos, pos2, size, size2;
			m_parent->GetPosition(pos);
			m_parent->GetSize(size);
			collider_parent->GetPosition(pos2);
			collider_parent->GetSize(size2);
			//We return false if there is clearly no collision
			if ((pos.x + size.x<pos2.x) || (pos.x > pos2.x + size2.x))
				return false;
			if ((pos.y + size.y<pos2.y) || (pos.y > pos2.y + size2.y))
				return false;
		}
	}

	std::vector<LineSegment> colliderSegments = polygon.GetSegments();
	for(unsigned int i=0; i<m_segments.size(); i++) {
		for( unsigned int j = 0; j < colliderSegments.size(); j++) {
			if (m_segments[i].Collision(colliderSegments[j])) {	
				return true;
			}
		}
	}
	return false;
}

std::vector<LineSegment>& CollisionPolygon::GetSegments()
{
	return m_segments;
}

void CollisionPolygon::SetPosition(glm::vec2&pos)
{
	for(unsigned int i = 0; i < m_segments.size(); i++) {
			m_segments[i].m_start += (pos-m_position);
			m_segments[i].m_end += (pos-m_position);
	}
	m_position = pos;
}