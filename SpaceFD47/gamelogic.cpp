
#include "base.h"
#include "masterutil.h"

#include "Player.h"
#include "menu.h"
#include "Physics.h"
#include "light.h"
#include "setting.h"
#include "Animation.h"
#include "collision.h"
#include "Text.h"
#include "NPCFighter.h"
#include "Sound.h"
#include "Map.h"
#include "KeyframeAnimation.h"

static void SpawnGame(void*arg, void *callerArg)
{
	//Start with the Meinmenu
	Sprite* black_base = new Sprite("black", "base");
	setting<std::string>menufile("mainmenu");
	MapObject::Setup();
	menu* mainmenu = new menu(menufile.GetSetting().c_str());
	
	//Create the physx system
	Physics *physics = new Physics;
	physics->SetAsActiveSystem();
	World world;
	world.SetAmbientColor(glm::vec4(0, 0, 0, 1));
	world.SetGravity(glm::vec2(glm::vec2(0, 0)));
	world.SetWorldUnits(glm::vec2(0.01, 0.01));
	physics->SetWorld(world);
}
Callback g_spawnGameCallback(SpawnGame, "spawnEngine");


static void StartNewGame(void*arg, void *callerArg)
{
	setting<std::string> startmenuPath("mainmenu");
	setting<std::string> startmenuName("name", startmenuPath.GetSettingFast().c_str());
	RemoveMenu("mainmenu");
	
	//Player *testani = new Player("baseplayer");
	//testani->Run();
	////testani->SetSize(glm::vec2(0.1, 0.1));

	//Sound::SetListenerVel(glm::vec3(0));
	//Sound::SetListenerPos(glm::vec3(0));

	//SoundSource testsound;
	//testsound.Load("test");
	//testsound.Play();
	//Sound::AddSource(testsound);

	////PLAY_SOUND("test");
	//NPCFighter *testnpc = new NPCFighter();
	//testnpc->Run();
	//testnpc->SetSize(glm::vec2(0.1, 0.1));
	//

	//light* testlight = new light("light_color");
	//testlight->SetPosition(glm::vec2(0.2, 0.8));
	//testlight->SetColor(glm::vec3(0.8, 1, 0.8));
	//testlight->SetStrength(1);
	//testlight->SetRadius(0.4f);

	//Sprite *testbg = new Sprite("space_bg_00", "base", 0);
	Map* level0 = new Map("level0");

	//KeyframeAnimation("testanimation");
}
Callback g_newGameCallback(StartNewGame, "newgame");

static void SettingsMenu(void*arg, void *callerArg)
{
	setting<std::string> startmenuPath("mainmenu");
	setting<std::string> startmenuName("name", startmenuPath.GetSettingFast().c_str());

	RemoveMenu(startmenuName.GetSetting());

	setting<std::string> settingMenuPath("settingmenu");
	menu* settingmenu = new menu(settingMenuPath.GetSetting().c_str());
}
Callback g_settingsCallback(SettingsMenu, "settings");
