#pragma once
#include "Sprite.h"
#include "Animation.h"

#include "collision.h"
#include "Physics.h"
#include "time.h"

#include "BasePlayer.h"
#include "BaseWeapon.h"

class Player :
	public BasePlayer
{
public:
	Player(std::string name);
	virtual ~Player(void);

	virtual void BeginPrimaryAttack();
	virtual void PrimaryAttack();
	virtual void EndPrimaryAttack();
	virtual void SecondaryAttack();
	virtual void Update(float timeElapsed) override;

private:
	float		m_speed;

	float		m_regenerateTime;
	float		m_regenerateRate;
	
	float		m_armor;
	float		m_armorRegenerateTime;
	float		m_armorRegenerateRate;

	glm::vec2	m_weaponOffset;
	glm::vec2	m_maxSpeed;
	
	
	TimerIdent	m_paTimer;
	TimerIdent	m_inputActionTimer;
	PhysicsIdent m_physical;
	
	bool m_primaryWasFired;
};

extern BaseClass * MapCreator_Player(std::vector<string> arguments);
