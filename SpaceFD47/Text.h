#pragma once
#include "Sprite.h"

struct letter
{
	TextureIdent texture;
	glm::vec2 size;
	glm::vec2 pos;
};

class Text :
	public Sprite
{
public:
	Text(std::string text, std::string font, std::string shader, glm::vec2 pos, float size);
	virtual ~Text();

	virtual void Render();
	virtual void DrawCallback(GlProgram&program);
	virtual void Create();

	virtual void SetColor(glm::vec4 color);
	virtual void GetColor(glm::vec4&color);

	virtual void SetText(std::string text);
	virtual void GetText(std::string&text);

	//Interaction functions

	virtual bool IsHovered();
	virtual bool IsClicked(int button);

private:
	bool			m_useable;
	std::string		m_fontname;
	std::vector<letter> m_letters;
	FT_Face			m_ftFace;
	std::string		m_text;
	float			m_TextSize;
	glm::vec4		m_color;
	bool			m_created;

	//For interaction with the text
	glm::vec2		m_minpos;
	glm::vec2		m_maxpos;
};

