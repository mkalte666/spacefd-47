/*
File: BaseClass.cpp
Purpose: Contains functions for the BaseClass
Author(s): Malte Kie�ling (mkalte666)
*/

#include "BaseClass.h"
#include "masterutil.h"
#include "SceneManager.h"

/*
* @name: BaseClass::BaseClass
* @return: none
* @description: Constructor that registers a baseclass-object in the scenemanager
* @notes: BaseClass-childs should call this. Also, only create BaseClass-instances via new
*/
BaseClass::BaseClass(void)
: m_subobjects()
{
	m_objId = SceneManager::AddObject(*this);
	m_type = 1;
	m_parent = nullptr;
}

/*
* @name: BaseClass::~BaseClass
* @return: none
* @description: Virtual destructor that removes the object from the SceneManager
* @notes: Should be called from child-classes destructors
*/
BaseClass::~BaseClass(void)
{
	//Drop subobjects, too
	for (auto obj : m_subobjects) {
		obj->SetParent(nullptr);
		SceneManager::Drop(obj);
	}
	//And remove from parent
	if (m_parent != NULL) {
		m_parent->FreeSubobject(this);
	}

	//and then remove us from the sceneManager
	SceneManager::RemoveObject(m_objId);
}

/*
* @name: BaseClass::Update
* @return: void
* @param: float timeElapsed: time in seconds since last call of Update
* @description: Virtual empty function body for Update of objects
* @notes: 
*/
void BaseClass::Update(float timeElapsed)
{

}

/*
* @name: BaseClass::Render
* @return: none
* @description: Virutal empty function body for Rendering of objects
* @notes:
*/
void BaseClass::Render()
{

}

/*
* @name: BaseClass::SetType
* @return: void
* @param: uint32_t type: type to set this to
* @description: Objects can have different types. Tyes are Bitflags, that are set in a uint32_t. 
* @notes:
*/
void BaseClass::SetType(uint32_t type)
{
	m_type = type;
}

/*
* @name: BaseClass::GetType
* @return: uint32_t
* @description: Returns the type of the Object.
* @notes: See BaseClass::SetType
*/
uint32_t BaseClass::GetType()
{
	return m_type;
}

/*
* @name: BaseClass::AddSubobject
* @return: void
* @params:	BaseClass* obj: newly created object (or not newly created who knows) to add to this instance
* @description: Adds a subobject to this class.
* @notes: Subobjects will be dropped in case this class will be dropped
*/
void BaseClass::AddSubobject(BaseClass* obj)
{
	if (obj == NULL || obj == this)
		return;
	m_subobjects.push_back(obj);
	obj->SetParent(this);
}

/*
* @name: BaseClass::FreeSubobject
* @return: void
* @params:	BaseClass* obj:  object that will no longer be part of this object
* @description: Adds a subobject to this class.
* @notes: Freed subobject will no longer be dropped in case this class will be dropped
*/
void BaseClass::FreeSubobject(BaseClass* obj)
{
	if (obj == NULL || obj == this)
		return;

	for (auto i = m_subobjects.begin(); i != m_subobjects.end(); i++) {
		if ((*i) == obj) {
			i = m_subobjects.erase(i);
			obj->SetParent(nullptr);
			return;
		}
	}
}

/*
* @name: BaseClass::DropSubobject
* @return: void
* @params:	BaseClass* obj:  object that will no longer be part of this object and should be dropped
* @description: Adds a subobject to this class.
* @notes: Freed subobject will no longer be dropped in case this class will be dropped
*/
void BaseClass::DropSubobject(BaseClass* obj)
{
	if (obj == NULL || obj == this)
		return;

	for (auto i = m_subobjects.begin(); i != m_subobjects.end(); i++) {
		if ((*i) == obj) {
			SceneManager::Drop(obj);
			obj->SetParent(nullptr);
			i = m_subobjects.erase(i);
			return;
		}
	}
}

/*
* @name: BaseClass:GetSubobjects
* @return: std::vector<BaseClass*> That contains all subobjects of this class
* @description: Returns the Parent of this object
* @notes: see also in BaseClass::AddSubobject
*/
std::vector<BaseClass*> BaseClass::GetSubobjects()
{
	return m_subobjects;
}

/*
* @name: BaseClass:SetParent
* @return: void
* @params: BaseClasss* obj: object that will be the parent of this object
* @description: Sets the Parent of this object
* @notes: see also in BaseClass::AddSubobject
*/
void BaseClass::SetParent(BaseClass* parentObj)
{
	if (m_parent != nullptr)
		m_parent->FreeSubobject(this);
	m_parent = parentObj;
}

/*
* @name: BaseClass:GetParent
* @return: BaseClass* to the parent object of this class
* @description: Returns the Parent of this object
* @notes: see also in BaseClass::SetParent
*/
BaseClass* BaseClass::GetParent()
{
	return m_parent;
}
