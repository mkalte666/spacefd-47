#pragma once

#include "Animation.h"
#include "Physics.h"


class BasePlayer :
	public Animation
{
public:
	BasePlayer(std::string name);
	virtual ~BasePlayer();

	virtual void Update(float timeElapsed);

	virtual void SetHealth(float health);
	virtual float GetHealth();

	virtual void PrimaryAttack() {  }
	virtual void SecondaryAttack() { }
private:
	float		m_health;
};

