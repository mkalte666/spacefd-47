#include "Map.h"
#include "setting.h"
#include "SceneManager.h"
#include "Interaction.h"

Map::Map(std::string name)
: m_running(false), m_name(name)
{

	//This thing parses the mapfile. Parsing it isnt that complicated.
	//The mainpart happens in the mapblocks, so we only split the file in the blocks.
	//then ecexute the mainblock (wich should create all the startup-stuff).
	setting<std::string> mapfiledir("mapdir");
	std::string mapfilename = mapfiledir.GetSettingFast() + name + ".map";
	ifstream	mapfile(mapfilename, std::ios::in);
	std::cout << "INFO: Loading " << mapfilename << std::endl;
	if (!mapfile.is_open()) {
		std::cout << "Could not load \"" << mapfilename << "\"!" << std::endl;
		return;
	}

	//File is parsed ONCE, line by line.
	char tmpbuffer[512];
	while (!mapfile.eof()) {
		mapfile.getline(tmpbuffer, 512);
		std::string line = tmpbuffer;
		//At first skipp comment and empty lines. they start with '#'
		if (line[0] == '#')
			continue;
		if (line[0] == '\n')
			continue;
		//now search for a [var]-pattern. the following lines are the var-block.
		//Its the only block that does not get parsed by a mapblock-structure.
		//Insted, it goes thrue the "varname = startinit"-lines and creates the variables
		if (line.find("[var]") != std::string::npos) {
			bool exit = false;
			while (!exit) {
				mapfile.getline(tmpbuffer, 512);
				line = tmpbuffer;
				if (line[0] == '\n')
					continue;
				if (line.find("[end]") != std::string::npos) {
					exit = true;
					continue;
				}
				std::string left;
				std::string right;
				int pos;
				if ((pos = line.find('=')) == std::string::npos) {
					std::cout << "ERROR IN " << name << "!" << std::endl;
					return;
				}
				left = line.substr(0, pos - 1);
				right = line.substr(pos + 1, line.length() - pos -1);
				stringstream leftout(left);
				stringstream rightout(right);
				bool has_only_digits = (right.find_first_not_of(" 0123456789.") == string::npos);
				if (has_only_digits) {
					float num = 0.0f;
					rightout >> num;
					m_numeric_vars[leftout.str()] = num;
				}
				else {
					m_string_vars[leftout.str()] = rightout.str();
				}
			}
			continue;
		}
		//Next thing is that we split the blocks. hard sayed, easy doing:
		//Find blocks beginning wihth [name], ending with [end], put them into mapblock
		int header_begin, header_end;
		if ((header_begin = line.find("[")) != std::string::npos
		&& (header_end = line.find("]")) != std::string::npos) {
			std::string name = line.substr(header_begin+1, header_end - header_begin - 1);
			mapblock newblock;
			newblock.name = name;
			bool exit = false;
			while (!exit) {
				mapfile.getline(tmpbuffer, 512);
				line = tmpbuffer;
				if (line[0] == '\n')
					continue;
				if (line.find("[end]") != std::string::npos) {
					exit = true;
					continue;
				}
				newblock.raw_content.push_back(line);
			}
			m_blocks[newblock.name] = newblock;
		}
	}
}

void Map::Update(float timeElapsed)
{
	if (!m_running) {
		if (m_blocks.find("mapentry") == m_blocks.end()) {
			std::cout << "ERROR: Could not find map entry in " << m_name << "!" << std::endl;
			SceneManager::Drop(this);
			return;
		}
		m_running = true;
		m_blocks["mapentry"].Execute(this);
	}

	Message rcv;
	while (MessageManager::PollMessage(this, rcv)) {
		if (rcv.type == ACTION_DESTROYED) {
			for (auto obj = m_objects.begin(); obj != m_objects.end(); obj++) {
				if (obj->m_object == rcv.source) {
					if (m_blocks.find(obj->m_onDestroyBlock) != m_blocks.end()) {
						m_blocks[obj->m_onDestroyBlock].Execute(this);
					}
					obj = m_objects.erase(obj);
					break;
				}
			}
		}
	}

	std::list<Message> Messagelist;
	for (auto obj : m_objects) {

		if (obj.m_onCollideBlock == "" && obj.m_onDamageBlock == "")
			continue; //nothing to do here...

		if (MessageManager::CopyListSilent(obj.m_object, Messagelist)) {
			for (auto msg : Messagelist) {
				switch (msg.type) {
					case ACTION_COLLIDE:
						if (m_blocks.find(obj.m_onCollideBlock) != m_blocks.end()) {
							m_blocks[obj.m_onCollideBlock].Execute(this);
						}
						break;
					case ACTION_ATTACK:
						if (m_blocks.find(obj.m_onDamageBlock) != m_blocks.end()) {
							m_blocks[obj.m_onDamageBlock].Execute(this);
						}
						break;
					default:

						break;
				}
			}
		}
	}
}

Map::~Map()
{
	m_objects.clear();
}

std::map<std::string, float> & Map::NumVars()
{
	return m_numeric_vars;
}

std::map<std::string, std::string> & Map::StringVars()
{
	return m_string_vars;
}

std::vector<MapObject>& Map::Objects()
{
	return m_objects;
}
std::map < std::string, mapblock>& Map::Blocks()
{
	return m_blocks;
}

//THis function does the whole parsing magic. Also, it uses the types in maptypes.h (the magic structures, functions, ...) to dynamicly create objects
bool mapblock::Execute(Map*parent)
{
	//Parsing happens line by line
	for (auto i = raw_content.begin(); i != raw_content.end(); i++) {
		string line = *i;
		if (line[0] == '\n' || line[0] == '#')	//Noone needs emptylines or comments in the parser
			//this is not needed but... just to be shure.
			continue;

		int pos = 0;
		//KEYWORD create
		if ((pos=line.find("create ")) != line.npos) {
			if ((pos = line.find(" ")) != line.npos) {
				std::string createline = line.substr(pos + 1, line.length() - pos - 1);
				MapObject newObj(nullptr);
				bool bShouldAddToLocalCollection = Create(createline, newObj);
				parent->Objects().push_back(newObj);
				if (bShouldAddToLocalCollection)
					parent->AddSubobject(newObj.m_object);
			}
		}
		//KEYWORD inc
		else if ((pos = line.find("inc ")) != line.npos){
			std::string right;
			pos = line.find_first_of(" ", pos);
			pos = line.find_first_not_of(" ", pos);
			int endpos = pos;
			endpos = line.find_first_of(" \n\0", pos);
			right = line.substr(pos, endpos -pos);
			stringstream rightoutstream(right);	
			std::string rightout;
			rightoutstream >> rightout;
			float num = 0.0f;
			if (parent->NumVars().find(rightout) != parent->NumVars().end()) {
				parent->NumVars()[rightout]++;
				}
			else {
				std::cout << "ERROR: Trying to increase non-numeric type or invalid name in " << name << std::endl;
				return false;
			}
		}
		//KEYWORD dec
		else if ((pos = line.find("dec ")) != line.npos){
			std::string right;
			right = line.substr(pos + 1, line.length() - pos - 1);
			stringstream rightout(right);
			bool has_only_digits = (right.find_first_not_of(" 0123456789.") == string::npos);

			float num = 0.0f;
			if (parent->NumVars().find(rightout.str()) != parent->NumVars().end()) {
				parent->NumVars()[rightout.str()]--;
			}
			else {
				std::cout << "ERROR: Trying to decrease non-numeric type or invalid name in " << name << std::endl;
				return false;
			}
		}
		//KEYWORD loadmap
		else if ((pos = line.find("loadmap ")) != line.npos){
			std::string right;
			pos = line.find_first_of(" ", pos);
			pos = line.find_first_not_of(" ", pos);
			int endpos = pos;
			endpos = line.find_first_of(" \n\0", pos);
			right = line.substr(pos, endpos - pos);
			Map* newmap = new Map(right);
			SceneManager::Drop(parent);
			return true;
		}
		//KEYWORD call
		else if ((pos = line.find("call ")) != line.npos){
			std::string right;
			pos = line.find_first_of(" ");
			pos = line.find_first_not_of(" ", pos);
			right = line.substr(pos, line.length() - pos);
			if (parent->Blocks().find(right) != parent->Blocks().end()) {
				parent->Blocks()[right].Execute(parent);
			}
			else {
				Engine::RunCallback(right);
			}
		}
		//KEYWORD if
		else if ((pos = line.find("if ")) != line.npos){
			//Parse the if line: (if [op1] [operator] [op2] [action])
			std::string lop, rop, op, action;
			pos = line.find_first_of(" ", pos);
			pos = line.find_first_not_of(" ", pos);
			int endpos = 0;
			endpos = line.find_first_of(" ", pos);
			lop = line.substr(pos, endpos - pos);

			pos = line.find_first_not_of(" ", endpos);
			endpos = line.find_first_of(" ", pos);
			op = line.substr(pos, endpos - pos);

			pos = line.find_first_not_of(" ", endpos);
			endpos = line.find_first_of(" ", pos);
			rop = line.substr(pos, endpos - pos);

			pos = line.find_first_not_of(" ", endpos);
			endpos = line.find_first_of(" ", pos);
			action = line.substr(pos, endpos - pos);

			bool lopIsNumVar = (parent->NumVars().find(lop) != parent->NumVars().end());
			bool lopIsStringVar = ((!lopIsNumVar) && (parent->StringVars().find(lop) != parent->StringVars().end()));
			bool lopIsNumeric = ((!lopIsNumVar) && (!lopIsStringVar) && lop.find_first_not_of(" 0123456789.") == string::npos);
			bool ropIsNumVar = (parent->NumVars().find(rop) != parent->NumVars().end());
			bool ropIsStringVar = ((!ropIsNumVar) && (parent->StringVars().find(rop) != parent->StringVars().end()));
			bool ropIsNumeric = ((!ropIsNumVar) && (!ropIsStringVar) && rop.find_first_not_of(" 0123456789.") == string::npos);
			//ignore nonsense-cases and only parse actual sensefull situations
			if (lopIsNumVar&&!ropIsNumeric&&!ropIsNumVar
				|| lopIsStringVar&&!ropIsStringVar
				|| lopIsNumeric&&!ropIsNumeric&&!ropIsNumVar
				|| !lopIsNumeric&&!lopIsNumVar&&!lopIsStringVar&&!ropIsStringVar
				)
				continue;

			bool isTrue = false;
			bool numericOperation = (lopIsNumVar || lopIsNumeric);
			float resultLop, resultRop;
			std::string resultStringLop, resultStringRop;
			//Fill vars for left...
			if (lopIsNumVar) {
				resultLop = parent->NumVars()[lop];
			}
			else if (lopIsStringVar) {
				resultStringLop = parent->StringVars()[lop];
			}
			else if (lopIsNumeric) {
				std::stringstream floatstream(lop);
				floatstream >> resultLop;
			}
			else {
				resultStringLop = lop;
			}
			//And right operand
			if (ropIsNumVar) {
				resultRop = parent->NumVars()[rop];
			}
			else if (ropIsStringVar) {
				resultStringRop = parent->StringVars()[rop];
			}
			else if (ropIsNumeric) {
				std::stringstream floatstream(rop);
				floatstream >> resultRop;
			}
			else {
				resultStringRop = rop;
			}
			
			//now do the operator-magic:
			if (op.find("==") != op.npos) {
				isTrue = (numericOperation ? (resultLop == resultRop) : (resultStringLop == resultStringLop));
			}
			else if (op.find("<=") != op.npos) {
				isTrue = (numericOperation ? (resultLop <= resultRop) : (resultStringLop.length() <= resultStringLop.length()));
			}
			else if (op.find(">=") != op.npos) {
				isTrue = (numericOperation ? (resultLop >= resultRop) : (resultStringLop.length() >= resultStringLop.length()));
			}
			else if (op.find("<") != op.npos) {
				isTrue = (numericOperation ? (resultLop < resultRop) : (resultStringLop.length() < resultStringLop.length()));
			}
			else if (op.find(">") != op.npos) {
				isTrue = (numericOperation ? (resultLop > resultRop) : (resultStringLop.length() > resultStringLop.length()));
			}
			else if (op.find("!=") != op.npos) {
				isTrue = (numericOperation ? (resultLop != resultRop) : (resultStringLop != resultStringLop));
			}
			else {
				std::cout << "ERROR: Invalid operator in " << line << std::endl;
			}

			if (isTrue) {
				//if is ture, we want to execute the block
				if (parent->Blocks().find(action) != parent->Blocks().end()) {
					parent->Blocks()[action].Execute(parent);
				}
				else {
					std::cout << "Invalid block given in if-statement! " << line << std::endl;
				}
			}
		}
		//KEYWORD '=' - assigns value (or variable) at variable
		else if ((pos = line.find("=")) != line.npos){
			//split right and left of '='
			string left, right;
			left = line.substr(0, pos - 1);
			right = line.substr(pos + 1, line.length() - pos - 1);
			stringstream leftout(left);
			std::string varname, rname;
			leftout >> varname;

			stringstream rightout(right);
			rightout >> rname;
			bool right_has_only_digits = (right.find_first_not_of(" 0123456789.") == string::npos);
			if (parent->StringVars().find(varname) != parent->StringVars().end()) {
				if (parent->StringVars().find(rname) != parent->StringVars().end()) {
					parent->StringVars()[varname] = parent->StringVars()[rname];
				}
				else {
					leftout >> parent->StringVars()[varname];
				}
			}
			else if (parent->NumVars().find(varname) != parent->NumVars().end()) {
				if (!right_has_only_digits) {
					if (parent->NumVars().find(rname) != parent->NumVars().end()) {
						parent->NumVars()[varname] = parent->NumVars()[rname];
					}
					else {
						std::cout << "ERROR: trying to assign non-numeric type at numeric variable in Map" << std::endl;
						return false;
					}
				}
			}
		}
	}
	return true;
}