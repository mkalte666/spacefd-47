#pragma once
#include "Sprite.h"
#include "Physics.h"
#include "Sound.h"

class Projectile :
	public Sprite
{
public:
	Projectile(std::string type, glm::vec2 pos, glm::vec2 vel);
	virtual ~Projectile();

	virtual void Update(float timeElapsed);

private:
	PhysicsIdent	m_physics;
	float			m_damage;

	SoundSourceIdent     m_emitSound;
	SoundSourceIdent	 m_hitSound;
	SoundSource			m_hitsoundSource;
};

