/*
File: menu.h
Purpose: class for menus
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once
#include "BaseClass.h"
#include "masterutil.h"
#include "Text.h"

extern void RemoveMenu(std::string menuname);

/* 
* @name: menu
* @parent: BaseClass
* @description: A class that shows a menu on the screen. 
*/
class menu :
	public BaseClass
{
public:
	menu(const char* config);
	virtual ~menu(void);

	virtual void Update(float timeElapsed);
	virtual void Cleanup();
private:
	Sprite* m_bgImage;
	std::vector<std::auto_ptr<Text>> m_texts;
	std::vector<std::string> m_idents;
	std::vector<std::string> m_ElementNames;
	std::string m_name;
	std::string m_removeCallbackName;
	float m_alphaIdle;
	float m_alphaHover;
	glm::vec2 m_centerpos;
	glm::vec2 m_elementSize;
};

