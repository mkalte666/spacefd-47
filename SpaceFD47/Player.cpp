#include "Player.h"
#include "masterutil.h"
#include "time.h"
#include "Physics.h"
#include "Interaction.h"
#include "UI.h"
#include "SceneManager.h"
#include "Projectile.h"

Player::Player(std::string name)
: BasePlayer(name)
{
	BasePlayer::BasePlayer(name);
	setting<string> psettings("psettings");
	setting<float> raw_boud("collision", psettings.GetSetting());
	setting<float> size("size", psettings.GetSetting());
	setting<float> mass("mass", psettings.GetSetting());
	setting<float> offset("weaponOffset", psettings.GetSettingFast());
	setting<float> maxSpeed("maxSpeed", psettings.GetSettingFast());
	setting<float> health("health", psettings.GetSettingFast());
	SetSize(glm::vec2(size.GetSettingFast(), size.GetSettingFast()));

	SetHealth(health.GetSettingFast());

	m_physical = Physics::GetActiveSystem().AddObj(new CollisionPolygon(raw_boud, this), this);
	m_physical->mass = mass.GetSettingFast();
	
	std::vector<float> tmpoff = offset.GetSettingArray();
	glm::vec2 vsize;
	GetSize(vsize);
	if (tmpoff.size() == 2) {
		m_weaponOffset = glm::vec2(tmpoff[0] * vsize.x, tmpoff[1]*vsize.y);
	}
	else {
		m_weaponOffset = glm::vec2(0);
	}
	std::vector<float> maxSpeedVec = maxSpeed.GetSettingArray();
	m_maxSpeed = glm::vec2(maxSpeedVec[0], maxSpeedVec[1]);
	m_inputActionTimer = SceneManager::GetTime().RegisterTimer(0.016f, true);
	m_paTimer = SceneManager::GetTime().RegisterTimer(0.3f, true);

	m_primaryWasFired = false;
	SetType(TYPE_PLAYER);
}


Player::~Player(void)
{
	Physics::GetActiveSystem().RemoveObj(this);
	BasePlayer::~BasePlayer();
}

void Player::Update(float timeElapsed)
{
	if (!m_inputActionTimer->m_running) {
		SceneManager::GetTime().RemoveTimer(m_inputActionTimer);
		if (UI::GetKey(GLFW_KEY_W)) m_physical->v.y = 0.5f;
		else if (UI::GetKey(GLFW_KEY_S)) m_physical->v.y = -0.5f;
		else m_physical->v.y = 0.0f;
		if (UI::GetKey(GLFW_KEY_A)) m_physical->v.x = -0.5f;
		else if (UI::GetKey(GLFW_KEY_D)) m_physical->v.x = 0.5f;
		else m_physical->v.x = 0.0f;
		if (UI::GetKey(GLFW_KEY_K)) BeginPrimaryAttack();
		else EndPrimaryAttack();
		m_inputActionTimer = SceneManager::GetTime().RegisterTimer(0.016f, true);
	}
	//Dfuq normalize still causes problems when lenght == 0...

	/*float len = m_physical->v.length();
	if (m_physical->v.length() > EPSILON && m_physical->v.length() >= m_maxSpeed.length()) {
		m_physical->a = glm::vec2(0);
		m_physical->v = glm::normalize(m_physical->v);
		m_physical->v *= m_maxSpeed.length();
	}*/

	//ATTACK!
	if (m_primaryWasFired)
		PrimaryAttack();

	//Check that we are still on the screen. Otherwise make shure we arnt moving there anymore
	glm::vec2 pos;
	GetPosition(pos);
	if (pos.x > 1) {
		pos.x = 1;
		m_physical->v.x = 0;
	}
	if (pos.y > 1){
		pos.y = 1;
		m_physical->v.y = 0;
	}
	if (pos.x < 0) {
		pos.x = 0;
		m_physical->v.x = 0;
	}
	if (pos.y < 0) {
		pos.y = 0;
		m_physical->v.y = 0;
	}
	SetPosition(pos);
	Message msg;
	while (MessageManager::PollMessage(this, msg)) {
		switch (msg.type)
		{
		case ACTION_ATTACK:
			SetHealth(GetHealth() - msg.fArg);
			break;
		case ACTION_COLLIDE:

			break;
		default:
			break;
		}
	}

	//We must comminicate some thigs to the rest of the engine.
	//Every npc should know, wehre we are, so there is a action and we use it
	Message posMessage;
	GetPosition(posMessage.posArg);
	posMessage.source = this;
	posMessage.type = ACTION_PLAYER_MOVED;
	MessageManager::Brodcast(posMessage);

	//See if we die
	if (GetHealth() <= 0) {
		//Shit wer're dead. Kill ourself. And let it look good!
		SceneManager::Drop(this);
		Animation* explosion = new Animation("explosion_1", "explosion_1", "base", 1);
		glm::vec2 size, pos;
		GetSize(size);
		GetPosition(pos);
		explosion->SetSize(size);
		explosion->SetPosition(pos);
		explosion->Run(1.0f);
		//Oh, and send a message so the game can react
		Message destrMessage;
		destrMessage.source = this;
		destrMessage.destination = NULL;
		destrMessage.type = ACTION_PLAYER_DESTROYED;
		MessageManager::Brodcast(destrMessage);
		return;
	}

	BasePlayer::Update(timeElapsed);
}

void Player::PrimaryAttack()
{
	if (!m_paTimer->m_running) {
		SceneManager::GetTime().RemoveTimer(m_paTimer);
		m_paTimer = SceneManager::GetTime().RegisterTimer(0.3f, true);
		glm::vec2 pos;
		GetPosition(pos);
		pos += m_weaponOffset;
		Projectile *pr = new Projectile("base_projectile", pos, glm::vec2(-0.0, 0.5));
	}
}

void Player::BeginPrimaryAttack() 
{
	m_primaryWasFired = true;
}

void Player::EndPrimaryAttack()
{
	m_paTimer->m_running = false;
	m_primaryWasFired = false;
}
void Player::SecondaryAttack()
{
}

BaseClass * MapCreator_Player(std::vector<string> arguments)
{
	if (arguments.size() != 1) {
		std::cout << "ERROR IN PLAYER CREATION: to many or less arguments! " << std::endl;
		return NULL;
	}
	std::string name;
	int beginpos, endpos;
	beginpos = endpos = 0;
	//name argument
	beginpos = arguments[0].find_first_not_of(" \"");
	endpos = arguments[0].find_last_not_of(" \"");
	if (endpos == arguments[0].npos || beginpos == arguments[0].npos) {
		std::cout << "ERROR in sprite MapCreator - invalid arguments" << std::endl;
		return NULL;
	}
	name = arguments[0].substr(beginpos, endpos - beginpos+1);
	Player * newplayer = new Player(name);
	newplayer->Run();
	return newplayer;
}