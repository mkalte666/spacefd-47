/*
File: Animation.cpp
Purpose: Contains functions for the Animation-class
Author(s): Malte Kie�ling (mkalte666)
*/
#include "Animation.h"
#include "setting.h"
#include "masterutil.h"
#include "SceneManager.h"

/*
* @name: Animation::Animation
* @param:	animation: std::string that contains the name of the animation.anim file
*			texture: std::string that contains the name of the testure for this animation
*			shader:  std::string that contains the name of the shader that will be used
*			layer:	int on what layer to draw on. 
* @return:	void
* @description:	Creates an animation from a .anim file with the given texture
*/
Animation::Animation(std::string animation, std::string texture, std::string shader, int layer)
: Sprite(texture, shader, layer), m_timeToRun(0.0f)
{
	setting<std::string> anim_dir("animationdir");
	std::string filepath = anim_dir.GetSettingFast()+std::string(animation)+std::string(".ani");
	m_drawable=false;
	m_running=false;
	
	setting<float> durations_setting("frame_times", filepath.c_str());
	setting<int> order_setting("frame_order", filepath.c_str());
	setting<int> tilenum_setting("tilenum", filepath.c_str());
	setting<int> tiles_w("tilesw", filepath.c_str());
	setting<int> tiles_h("tilesh", filepath.c_str());
	
	std::vector<int> order = order_setting.GetSettingArray();
	std::vector<float> durations = durations_setting.GetSettingArray();
	if(order.size()!=durations.size()) {
		std::cout << "ERROR: Animation file '" << filepath << "' contains wrong array-sizes!" << std::endl;
		return;
	}
	
	m_tilesW = tiles_w.GetSettingFast();
	m_tilesH = tiles_h.GetSettingFast();
	m_tilenum = tilenum_setting.GetSettingFast();

	for(unsigned int i = 0; i < order.size(); i++) {
		m_frames.push_back(std::pair<int, float>(order[i], durations[i]));
	}
	m_activeFrame = m_frames.begin();
	m_drawable = true;
}

/*
* @name: Animation::~Animation
* @param:	void
* @return:	void
* @description: Delets the Animation
*/
Animation::~Animation(void)
{
	Sprite::~Sprite();
}

/*
* @name: Animation::Run
* @param:	time: float in seconds how long this animation will run
* @return:	void
* @description:	Starts the animation. if time ==0, it will runn until Stop() is called
*/
void Animation::Run(float time)
{
	if(!m_drawable) return;
	m_running = true;
	m_activeFrame = m_frames.begin();
	m_timer.Reset();
	m_frameTimer.Reset();
	m_timeToRun = time;
}

/*
* @name: Animation::Stop
* @param:	void
* @return:	void
* @description: Stops the animation and removes it from the scene manager
*/
void Animation::Stop()
{
	if(!m_drawable) return;
	m_running = false;
	m_timer.Reset();
	m_frameTimer.Reset();
	SceneManager::Drop(this);
}

/*
* @name: Animation::Update
* @param:	timeElapsed: float with the time in seconds since the last call
* @return:	void
* @description: Updates the animations, and if it it was started with Run(arg) arg!=0 stops it if it reached the stop-time
*/
void Animation::Update(float timeElapsed)
{
	if(m_running == false || m_drawable == false) return;

	if (m_timeToRun != 0.00f && m_timer.Get() >= m_timeToRun) {
		Stop();
		return;
	}

	if (m_frameTimer.Get() >= m_activeFrame->second) {
		m_activeFrame++;
		if(m_activeFrame == m_frames.end()) m_activeFrame = m_frames.begin();
		m_frameTimer.Reset();
	}
}

/*
* @name: Animation::Render()
* @param:	void
* @return:	void
* @description:	Draws the Animation.
*/
void Animation::Render()
{
	if(m_running == false || m_drawable == false) return;
	float w = (float)(1/(float)m_tilesW);
	float h = (float)(1/(float)m_tilesH);

	float x = (m_activeFrame->first%m_tilesW)/(float)m_tilesW;
	float y = h*(m_tilesH-1)-((m_activeFrame->first/m_tilesW)/(float)m_tilesH);
	
	SetOffset(glm::vec4(x,y,x+w,y+h));
	Sprite::Render();
}