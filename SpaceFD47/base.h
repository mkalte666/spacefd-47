﻿/*
File: base.h
Purpose: Contains all stl and libary headers that are needed nearly everywhere in the project. 
Author(s): Malte Kießling (mkalte666)
*/

#pragma once

#include "targetver.h"

#ifndef BASE_HEADER
#define BASE_HEADER

#define BUFFER_SIZE 512


#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <limits>
#include <assert.h>
#include <memory>
#include <algorithm>
#include <map>
#include <cmath>
#include <cstdlib>
#include <list>
#include <chrono>
#include <thread>
#include <mutex>
#include <regex>


#ifdef _WIN32
#include <winsock2.h>
#include <windows.h>
#endif
#ifdef _LINUX
typedef SOCKET int;
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#endif

#ifdef _WIN32

#define GLFW_DLL //Needed for linking to Dll!
#endif //WIN32

#include <GL\glew.h>
#include <GL\glfw3.h>
#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

//Sound
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

//Genaral media: ffmpeg
#pragma comment (lib, "avformat.lib")
#pragma comment (lib, "avcodec.lib")
#pragma comment (lib, "avutil.lib")
#pragma comment (lib, "swscale.lib")

extern "C"
{
#ifndef __STDC_CONSTANT_MACROS
#define __STDC_CONSTANT_MACROS
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libswscale\swscale.h>
};

//Font loading: FreeType2
#include <ft2build.h>
#include FT_FREETYPE_H


using namespace std;
#include <assert.h> 
#define ╯︵┻━┻ ,assert(0)
#define ╯°□° 0


/* The PP_NARG macro returns the number of arguments that have been
* passed to it.
*/

#define PP_NARG(...) \
	PP_NARG_(__VA_ARGS__, PP_RSEQ_N())
#define PP_NARG_(...) \
	PP_ARG_N(__VA_ARGS__)
#define PP_ARG_N( \
	_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, \
	_11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
	_21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
	_31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
	_41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
	_51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
	_61, _62, _63, N, ...) N
#define PP_RSEQ_N() \
	63, 62, 61, 60, \
	59, 58, 57, 56, 55, 54, 53, 52, 51, 50, \
	49, 48, 47, 46, 45, 44, 43, 42, 41, 40, \
	39, 38, 37, 36, 35, 34, 33, 32, 31, 30, \
	29, 28, 27, 26, 25, 24, 23, 22, 21, 20, \
	19, 18, 17, 16, 15, 14, 13, 12, 11, 10, \
	9, 8, 7, 6, 5, 4, 3, 2, 1, 0


#endif
