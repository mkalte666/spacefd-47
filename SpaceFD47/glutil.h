/*
File: gluitil.h
Purpose: Header for OpenGL utility functions
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once
#include "base.h"
extern GLuint LoadProgram(std::string vertex_file_path, std::string fragment_file_path);
extern void GenerateFramebuffer(int w, int h, GLuint &dstFboId, GLuint &dstTex, GLuint &dstDepth);
extern void SetNewTextureData(GLuint textureID, char * data, int width, int height, GLint format, GLenum type, bool filter = true);

unsigned int BufferFromImage(std::string path, unsigned char **dst, int &w, int &h);