#pragma once
#include "base.h"
#include "time.h"
#include "Sprite.h"
#include "Texture.h"
#include "BaseClass.h"

class SceneManager
{
private:
	SceneManager(void);
	virtual ~SceneManager(void);

public:
	static void Init();
	static void Destroy();

	static void Render();
	static void Update();

	static Texture* GetTexture(std::string name);
	static Texture* GenTexture(Texture *texture);
	static void		DelTexture(Texture *texture);

	static int						AddObject(BaseClass& obj);
	static void						RemoveObject(unsigned int obj);
	static void						RemoveObject(BaseClass* obj);
	static bool						VerifyObject(BaseClass* obj);
	static std::vector<BaseClass*>	GetAllObjects();
	static void						Drop(BaseClass* obj);

	static TimeManager& GetTime();

private:
	static std::vector<BaseClass*>	m_SceneElements;
	static std::vector<BaseClass*>	m_DropElements;
	static std::vector<Texture*>	m_Textures;
	static TimeManager				m_time;
	static Timer					m_masterTimer;
	static Timer					m_frameTimer;
	static int						m_generated_num;
	
protected:
	
};

