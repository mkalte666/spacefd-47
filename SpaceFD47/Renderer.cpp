#include "Renderer.h"
#include "glutil.h"
#include "setting.h"
#include "UI.h"

//Stativ vars
bool Renderer::m_init = false;
window* Renderer::m_renderWindow = NULL;
GLuint Renderer::m_vertexArray = 0;
GLuint Renderer::m_quadVertexBuffer = 0;
GLuint Renderer::m_activeFrameBuffer = 0;
std::list<RenderLayer> Renderer::m_layers = std::list<RenderLayer>();
GlProgram Renderer::m_quadProgram = GlProgram();
std::map<std::string, GLuint> Renderer::m_programs = std::map<std::string, GLuint>();
Effect* Renderer::m_firstEffect = NULL;

//Some globals that are ~needed~
//Represents a quad. Used for rendering
static const GLfloat g_quad[] = {
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	-1.0f, 1.0f, 0.0f,
	-1.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	1.0f, 1.0f, 0.0f,
};

bool Renderer::Init(unsigned int w, unsigned int h, std::string title)
{
	m_renderWindow = new window(w, h, title.c_str());
	m_firstEffect = NULL;
	//OpenGL setup

	//Render Setup
	//Vertex Buffer. We have 1 layout, so...
	glGenVertexArrays(1, &m_vertexArray);
	glBindVertexArray(m_vertexArray);
	glGenBuffers(1, &m_quadVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_quadVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad), g_quad, GL_STATIC_DRAW);
	m_activeFrameBuffer = 0;
	//Set our vertex layout
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_quadVertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDisableVertexAttribArray(0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	m_quadProgram = GlProgram("quadBase");

	//Last, we init the UI for this window.
	UI::Init(m_renderWindow);
	m_init = true;
	return m_init;
}

void Renderer::Destroy()
{
	for (auto i = m_programs.begin(); i != m_programs.end(); i++)
		glDeleteProgram(i->second);
}

void Renderer::Draw(void* texture, GlProgram&program, float* pos, float* size, float* offset, int layer, DrawCallback callback, void* callback_arg)
{
	if (!m_init) return;
	//Find the fbo for the layer to draw on
	_SetFboToLayer(layer);



	//Loactions of the args in the shader
	GLuint posID = program["position"];
	GLuint offID = program["offset"];
	GLuint sziID = program["size"];
	GLuint texID = program["textureSampler"];
	GLuint alpID = program["alpha"];
	GLuint layID = program["layer"];
	GLuint textureID = ((Texture*)texture)->GetTextureId();

	glBindFramebuffer(GL_FRAMEBUFFER, GetActiveFramebuffer());

	program.Use();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glUniform1i(textureID, 0);
	glUniform2f(posID, pos[0], pos[1]);
	glUniform4f(offID, offset[0], offset[1], offset[2], offset[3]);
	glUniform2f(sziID, size[0], size[1]);
	glUniform1f(alpID, ((Texture*)texture)->GetAlpha());
	glUniform1f(layID, static_cast<float>(layer));

	//Call callback (if exists)
	if (callback != NULL)
		callback(program, callback_arg);
	glEnableVertexAttribArray(0);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisableVertexAttribArray(0);
}


void Renderer::Render()
{
	//Select framebuffer
	if (m_firstEffect == NULL)
		m_activeFrameBuffer = 0;
	else
		m_activeFrameBuffer = m_firstEffect->fbo;
	//Render all layers
	glBindFramebuffer(GL_FRAMEBUFFER, m_activeFrameBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Draw all the layers
	for (auto i = m_layers.begin(); i != m_layers.end(); i++) {
		m_quadProgram.Use();
		GLuint texID = m_quadProgram["textureSampler"];
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, i->tex);
		glUniform1i(i->tex, 0);
		glEnableVertexAttribArray(0);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		glDisableVertexAttribArray(0);
		i->done = true;
	}

	if (m_firstEffect != NULL)
		m_firstEffect->Render();


	m_renderWindow->SwapBuffers();
}

GLuint Renderer::GetActiveFramebuffer()
{
	return m_activeFrameBuffer;
}

void Renderer::SetActiveFramebuffer(GLuint id)
{
	m_activeFrameBuffer = id;
}

void Renderer::AddEffectEnd(Effect* eff)
{
	if (m_firstEffect == NULL) m_firstEffect = eff;
	else m_firstEffect->Add(eff);
}

void Renderer::AddEffectBegin(Effect* eff)
{
	if (m_firstEffect == NULL) m_firstEffect = eff;
	else {
		eff->Add(m_firstEffect);
		m_firstEffect = eff;
	}
}

void Renderer::RemoveEffect(Effect* eff)
{
	if (m_firstEffect == NULL) return;
	if (m_firstEffect == eff) {
		m_firstEffect = NULL;
		return;
	}
	Effect* tmp = m_firstEffect;
	if (tmp == NULL)
		return;
	if (tmp->next == NULL)
		return;

	do {
		if (tmp->next == eff) {
			if (tmp->next->next == NULL) tmp->next = NULL;
			else tmp->next = tmp->next->next;
		}
		if (tmp->next == NULL)
			break;
		tmp = tmp->next;
	} while (tmp->next != NULL);
}



GLuint Renderer::GetProgram(std::string shadername)
{
	
	std::map<std::string, GLuint>::iterator pos = m_programs.find(shadername);
	if (pos != m_programs.end()) {
		return pos->second;
	}
	setting<std::string> shaderdir("shaderdir");
	std::string vshader(shaderdir.GetSetting());
	std::string fshader(shaderdir.GetSetting());
	vshader += shadername;
	vshader += ".vsh.glsl";
	fshader += shadername;
	fshader += ".fsh.glsl";
	GLuint programId = LoadProgram(vshader, fshader);

	m_programs.insert(std::pair<std::string, GLuint>(shadername, programId));
	return programId;
}





void Renderer::GenFramebuffer(GLuint&dstFbo, GLuint&dstTex, GLuint&dstDpth)
{
	GenerateFramebuffer(m_renderWindow->GetW(), m_renderWindow->GetH(), dstFbo, dstTex, dstDpth);
}


void Renderer::GetWindowDimensions(int&w, int&h)
{
	w = m_renderWindow->GetW();
	h = m_renderWindow->GetH();
}

void Renderer::_SetFboToLayer(int layer)
{
	if (m_layers.begin() == m_layers.end()) {
		RenderLayer tmp;
		tmp.done = false;
		GenFramebuffer(tmp.fbo, tmp.tex, tmp.dpth);
		tmp.layer = layer;
		m_layers.push_front(tmp);
		m_activeFrameBuffer = tmp.fbo;
		return;
	}
	auto i = m_layers.begin();
	for (; i != m_layers.end(); i++) {
		if (i->layer > layer) {
			RenderLayer tmp;
			tmp.done = false;
			GenFramebuffer(tmp.fbo, tmp.tex, tmp.dpth);
			tmp.layer = layer;
			i = m_layers.insert(i, tmp);
			m_activeFrameBuffer = tmp.fbo;
			return;
			break;
		} 
		else if(i->layer == layer){
			m_activeFrameBuffer = i->fbo;
			if (i->done) {
				i->done = false;
				glBindFramebuffer(GL_FRAMEBUFFER, i->fbo);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			}

			return;
			break;
		}
	}
	if (i == m_layers.end()) {
		RenderLayer tmp;
		tmp.done = false;
		GenFramebuffer(tmp.fbo, tmp.tex, tmp.dpth);
		tmp.layer = layer;
		i = m_layers.insert(i, tmp);
		m_activeFrameBuffer = tmp.fbo;
		return;
	}

	return;
}

//-----------------------------------------------------
//Effects
Effect::Effect(std::string programname)
: program(programname)
{
	next = NULL;
	Renderer::GenFramebuffer(fbo, tex, dpth);
	m_drawCallback = NULL;
	m_CallbackArg = NULL;
}

Effect::~Effect()
{

}

void Effect::Render()
{
	if (next == NULL)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else {
		glBindFramebuffer(GL_FRAMEBUFFER, next->fbo);
	}
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	program.Use();
	GLuint texID = program["textureSampler"];
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);
	glUniform1i(tex, 0);
	//If we have a callback, call it NOW
	if (m_drawCallback != NULL) m_drawCallback(program, m_CallbackArg);

	glEnableVertexAttribArray(0);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisableVertexAttribArray(0);

	if (next != NULL) next->Render();
}

void Effect::Add(Effect* eff)
{
	if (next == NULL) next = eff;
	else next->Add(eff);
}

void Effect::AddDrawCallback(EffectCallback callback, void* arg)
{
	m_drawCallback = callback;
	m_CallbackArg = arg;
}