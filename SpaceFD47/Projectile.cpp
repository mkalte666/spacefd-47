#include "Projectile.h"
#include "masterutil.h"
#include "SceneManager.h"
#include "Interaction.h"
#include "Sound.h"

Projectile::Projectile(std::string type, glm::vec2 pos, glm::vec2 vel) 
: Sprite("base")
{
	setting <std::string> settingpath("weaponcfg");
	std::string cfgfile = settingpath.GetSettingFast() + type + ".cfg";
	setting<float> raw_bound("collision", cfgfile);
	setting<float> mass("mass", cfgfile);
	setting<float> size("size", cfgfile);
	setting<float> damage("damage", cfgfile);
	setting<std::string> image("image", cfgfile);
	setting<std::string> emitSoundSetting("emitsound", cfgfile);
	setting<std::string> collideSoundSetting("collidesound", cfgfile);
	setting <std::string> shader("shader", cfgfile);

	//SoundSource emitSound, collideSound;
	//emitSound.Load(emitSoundSetting.GetSettingFast());
	//collideSound.Load(collideSoundSetting.GetSettingFast());
	//emitSound.Play();
	//m_emitSound = Sound::AddSource(emitSound);
	//m_hitSound = Sound::AddSource(collideSound);
	//m_hitsoundSource = Sound::GetSource(m_hitSound);

	SetPosition(pos);
	SetSize(glm::vec2(size.GetSettingFast()));
	SetOffset(glm::vec4(0, 0, 1, 1));
	m_physics = Physics::GetActiveSystem().AddObj(new CollisionPolygon(raw_bound, this), this);
	m_physics->mass = mass.GetSettingFast();
	m_physics->a = glm::vec2(0);
	m_physics->v = vel;
	m_damage = damage.GetSettingFast();
	//Sprite::Sprite(image.GetSettingFast(), shader.GetSettingFast(), 5);
	SetTexture(SceneManager::GetTexture(image.GetSettingFast()));
	SetLayer(1);
	SetType(TYPE_BULLET);
}


Projectile::~Projectile()
{
	Physics::GetActiveSystem().RemoveObj(this);
	//Sound::DelSource(m_emitSound);
	Sprite::~Sprite();
}

void Projectile::Update(float timeElapsed)
{
	glm::vec2 pos;
	GetPosition(pos);
	if (pos.x < 0 || pos.x > 1 || pos.y < 0 || pos.y > 1) {
		SceneManager::Drop(this);
		SetVisible(false);
		return;
	}

	Message msg;
	Message dmgMsg;
	while (MessageManager::PollMessage(this, msg)) {
		switch (msg.type) {
		case ACTION_COLLIDE:  //When we collide we destroy things (yeah!) damage players(uhh) and ourself (wait what?!?)
			dmgMsg.source = this;
			dmgMsg.type = ACTION_ATTACK;
			dmgMsg.fArg = m_damage;
			dmgMsg.iArg = 0;
			dmgMsg.destination = msg.source;
			MessageManager::Send(dmgMsg);
			
			//m_hitsoundSource.Play(); //We hit something so make noise!

			SceneManager::Drop(this);	//Kill ourself!
			
			SetVisible(false);
			return;
			break;

		default:
			break;
		}
	}
}
