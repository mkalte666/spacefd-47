/*
File: menu.cpp
Purpose: Contains the function for the class that handles menus. 
Author(s): Malte Kie�ling (mkalte666)
*/
#include "menu.h"
#include "setting.h"
#include "Text.h"
#include "UI.h"

void RemoveMenu(std::string menuname)
{
	menuname += "_remove";
	Engine::RunCallback(menuname.c_str());
}

/*
* @name: RemoveMenu
* @param:	arg: void* that should contain a pointer to a menu-class
* @return:	void
* @description:	Removes a menu. Used as Staic function for Callbacks. 
*/
void RemoveMenu(void* arg, void *pArg)
{
	menu* delmenu = static_cast<menu*>(arg);
	delete delmenu;
}

/*
* @name: menu::menu
* @param:	config: const char* that is the name of the config-file. 
* @return:	~
* @description: constructor of the menu-class. Creates A menu with the settings from a menu-cfg file
*/
menu::menu(const char* config)
{
	setting<std::string>bg_name("menu_bg", config);
	setting<std::string>name("name", config);
	setting<std::string>element_names_raw("element_names", config);
	setting<std::string>elements_callbacks_raw("element_calls", config);
	setting<int>num_elements("num_elements", config);
	setting<float>alphaIdle("alpha_idle", config);
	setting<float>alphaHover("alpha_hover", config);
	setting<float>element_w("element_w", config);
	setting<float>element_h("element_h", config);
	setting<float>centerpos_x("centerpos_x", config);
	setting<float>centerpos_y("centerpos_y", config);
	setting<std::string>font("font", config);

	m_alphaIdle = alphaIdle.GetSettingFast();
	m_alphaHover = alphaHover.GetSettingFast();
	m_centerpos = glm::vec2(centerpos_x.GetSettingFast(), centerpos_y.GetSettingFast());
	m_elementSize = glm::vec2(element_w.GetSettingFast(), element_h.GetSettingFast());
	m_name = name.GetSettingFast();
	m_ElementNames = element_names_raw.GetSettingArray();

	m_bgImage = new Sprite(bg_name.GetSettingFast().c_str(), "base");

	m_idents = elements_callbacks_raw.GetSettingArray();

	for(unsigned int i = 0; i < m_idents.size(); i++) {
		Text *tmp = new Text(m_ElementNames[i], font.GetSettingFast(), "text", m_centerpos, m_elementSize.y);
		tmp->SetSize(glm::vec2(m_elementSize.x, m_elementSize.y));
		tmp->SetPosition(glm::vec2(m_centerpos.x-m_elementSize.x*0.5, m_centerpos.y-(m_elementSize.y*0.5*m_idents.size())+(m_elementSize.y*i)));
		m_texts.push_back(std::auto_ptr<Text>(tmp));
	}
	m_removeCallbackName = m_name;
	m_removeCallbackName+="_remove";
	Engine::RegisterCallback(RemoveMenu, m_removeCallbackName.c_str(), this);
}

/*
* @name: menu::~menu
* @param:	void
* @return:	void
* @description: Destructor for menu-class
*/
menu::~menu(void)
{
	delete m_bgImage;
	for (unsigned int i = 0; i < m_texts.size(); i++) {
		delete m_texts[i].release();
	}
	Engine::RemoveCallback(m_removeCallbackName.c_str());
	BaseClass::~BaseClass();
}

/*
* @name: menu::Cleanup
* @param:	void
* @return:	void
* @description:	Delets the menu and Removes all containing stuff. 
*/
void menu::Cleanup(void)
{
	
}

/*
* @name: menu::Update
* @param:	timeElapsed: float that contains the time in seconds since the last call of Update
* @return:	void
* @description: Updates the menu, hilights hover-elements. such stuff.
*/
void menu::Update(float timeElapsed)
{
	double mx;
	double my;
	UI::GetCursor(mx, my);
	for( unsigned int i = 0; i < m_texts.size(); i++) {
		if (m_texts[i]->IsHovered()) {
			m_texts[i]->SetColor(glm::vec4(1, 1, 1, 1.0));
			if (m_texts[i]->IsClicked(0)) {
				while (UI::GetMousebutton(0));
				Engine::RunCallback(m_idents[i].c_str());
				break;
			}
		} else
			m_texts[i]->SetColor(glm::vec4(1, 1, 1, 0.5));
		
	}
	
}

