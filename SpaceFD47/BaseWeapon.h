#pragma once
#include "BaseClass.h"



class BaseWeapon :
	public BaseClass
{
public:
	BaseWeapon();
	virtual ~BaseWeapon();

	virtual void PrimaryAttack();
	virtual void SecondaryAttack();

	virtual void Update();
	virtual void Equip();
	virtual void Drop();
private:

};

