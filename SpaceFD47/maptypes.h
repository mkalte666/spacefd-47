#pragma once
#include "base.h"
#include "BaseClass.h"

typedef BaseClass*(*CreateCallback)(std::vector< std::string>);

struct MapType
{
	CreateCallback	callback;
	std::string		name;
	BaseClass * Call(std::vector<std::string>arg) { return callback(arg); }
};

struct MapObject
{
public:
	MapObject(BaseClass* object, std::string destroyBlock = "", std::string collideBlock = "", std::string damageBlock = "");
	virtual ~MapObject();
	static void Setup();
	BaseClass*			m_object;
	
public:
	std::string			m_onDestroyBlock;
	std::string			m_onCollideBlock;
	std::string			m_onDamageBlock;
	static std::map < std::string, MapType> m_creators;
};

extern bool Create(std::string createline, MapObject& newobj);

#define MAPTYPE_BEGIN MapType Makro_newtype;

#define MAPTYPE(Typename) Makro_newtype.callback = MapCreator_##Typename; \
	Makro_newtype.name = #Typename; \
	m_creators[Makro_newtype.name] = Makro_newtype;

#define MAPTYPE_END ;