#pragma once
#include "Texture.h"
#include "window.h"
#include "GlProgram.h"

typedef void(*DrawCallback)(GlProgram&, void*);
typedef void(*EffectCallback)(GlProgram&, void*);

class Effect;
struct RenderLayer;

class Renderer
{
private:

public:
	static bool Init(unsigned int w, unsigned int h, std::string title);
	static void Destroy();
	static void Draw(void* texture, GlProgram&program, float* pos, float* size, float* offset, int layer = 0, DrawCallback callback = NULL, void* callback_arg = NULL);
	static void Render();
	
	//Shader and buffer management
	static GLuint GetProgram(std::string shadername);
	static void GenFramebuffer(GLuint&dstFbo, GLuint&dstTex, GLuint&dstDpth);

	static void AddEffectBegin(Effect* effect);
	static void AddEffectEnd(Effect* effect);
	static void RemoveEffect(Effect* effect);

	//Informations
	static GLuint	GetActiveFramebuffer();
	static void	SetActiveFramebuffer(GLuint id);
	static void GetWindowDimensions(int&w, int&h);
private:
	static bool m_init;
	//window and context management
	static window		*m_renderWindow;

	//drawing
	static GLuint		m_vertexArray;
	static GLuint		m_quadVertexBuffer;
	static GLuint		m_activeFrameBuffer;

	//Layer
	static std::list<RenderLayer> m_layers;
	static GlProgram	m_quadProgram;
	//shader management
	static std::map<std::string, GLuint> m_programs;

	//Effect system
	static Effect		*m_firstEffect;
	//methods
protected:
	static void _SetFboToLayer(int layer);
};


//Effects
class Effect
{
public:
	GLuint fbo;
	GLuint tex;
	GLuint dpth;
	GlProgram program;
	Effect* next;

	Effect(std::string programname);

	~Effect();
	void Add(Effect* eff);
	void AddDrawCallback(EffectCallback callback, void* arg = NULL);
	void Render();

private:
	EffectCallback	m_drawCallback;
	void*			m_CallbackArg;
};

//Layer management
struct RenderLayer
{
	GLuint	fbo;
	GLuint	tex;
	GLuint	dpth;
	int		layer;
	bool done;
};