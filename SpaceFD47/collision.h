/*
File: collision.h
Purpose: Contains class definitions for the collision detection
Author(s): Malte Kie�ling (mkalte666)
*/
#pragma once
#include "base.h"
#include "setting.h"
#include "BaseClass.h"

//Floating point stuff is, well, wierd. We cant use 0 because rounding-errors are evil.
//So we wont ask if something is 0, we ask if something is verry small. 
//Verry small is 0.00001, so 
#define EPSILON 0.000000001 //Ok, we got even smaller :>

class LineSegment
{
public:
	LineSegment(void);
	LineSegment(glm::vec2&start, glm::vec2&end);
	~LineSegment(void);

	bool Collision(LineSegment&segment);

	glm::vec2	m_start;
	glm::vec2	m_end;
};

class CollisionPolygon
{
public:
	CollisionPolygon(BaseClass* parent = NULL);
	CollisionPolygon(std::vector<LineSegment> parts, BaseClass *parent = NULL);
	CollisionPolygon(setting<float>&setting, BaseClass *parent = NULL);
	
	
	~CollisionPolygon();

	void	Update();
	bool Collide(CollisionPolygon& polygon);
	void SetPosition(glm::vec2&pos);
	std::vector<LineSegment>& GetSegments();

	BaseClass* GetParent();
private:
	std::vector<LineSegment>	m_segments;
	BaseClass	*m_parent;
	glm::vec2	m_position;
};
