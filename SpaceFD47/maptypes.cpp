#include "maptypes.h"
#include "SceneManager.h"

//All headers with typecreators here
#include "BaseClass.h"
#include "Sprite.h"
#include "Player.h"
#include "NPCFighter.h"
#include "KeyframeAnimation.h"

std::map < std::string, MapType> MapObject::m_creators = std::map < std::string, MapType>();

MapObject::MapObject(BaseClass* object, std::string destroyBlock , std::string collideBlock , std::string damageBlock )
: m_object(object), m_onDestroyBlock(destroyBlock), m_onCollideBlock(collideBlock), m_onDamageBlock(damageBlock)
{
	
}

MapObject::~MapObject()
{
//	SceneManager::Drop(m_object);
}

bool Create(std::string createline, MapObject&newobj) {
	newobj = MapObject(nullptr);
	bool result = true;
	if (createline[0] == '\0' || createline[0] == '\n') {
		std::cout << "ERROR: invalid create line '" << createline << "'!" << std::endl;
		return false;
	}
	
	std::string creator, arguments;
	//First, we need to parse the creator. The Creator parsing table goes thru all tyoes and has, sometimes, even specific argumets.
	//Arguments in the creator are sepereted by ',', like this: create Sprite("arg1", "arg2", 0.4, "arg4")
	int pos = 0;
	int creatorendpos = 0;
	if ((pos = createline.find(' ')) == createline.npos) {
		std::cout << "ERROR: invalid create line '" << createline << "'!" << endl;
		return false;
	}
	if ((creatorendpos = createline.find(')')) != createline.npos) {
		pos = creatorendpos;
		creator = createline.substr(0, pos + 1);
	}
	else {
		creator = createline.substr(0, pos );
	}
	arguments = createline.substr(pos + 1, createline.length() - pos - 1);
	//Split the arguments of the creator
	std::vector<std::string> creatorArgs;

	std::string creatorRawArgs("");
	//If we have args ( '()' are existent) put them into creatorRawArgs
	if ((pos = creator.find("(")) != creator.npos) {
		int backpos;
		if ((backpos = creator.rfind(")")) != creator.npos) {
			creatorRawArgs = creator.substr(pos + 1, backpos - pos - 1);
		}
		//Also we want the creator to only be our type, so
		creator = creator.substr(0, pos);
	}
	if(creatorRawArgs != "") {
		//when we can split it, spilt it. else, we only have one argument.
		//Todo: split out strings
		if ((pos = creatorRawArgs.find(",")) != creator.npos) {
			do {
				std::string argstring = creatorRawArgs.substr(0, pos);
				creatorArgs.push_back(argstring);
				creatorRawArgs = creatorRawArgs.substr(pos + 1, creatorRawArgs.length() - pos - 1);
			} while ((pos = creatorRawArgs.find(",")) != creator.npos);
		}
		//else, AND AS LAST STEP from the stuff above, copy the whole thing into our arglist
		creatorArgs.push_back(creatorRawArgs);
	}

	//So now we have an argumentlist, now the name of the type, we can to the thing that creates the objects \o/
	BaseClass* obj = NULL;

	if (MapObject::m_creators.find(creator) != MapObject::m_creators.end()) {
		obj = MapObject::m_creators[creator].Call(creatorArgs);
		newobj = MapObject(obj);
		
		if (obj == NULL) return false;
		//In case we successfully created the object its time to parse the outer arguments
		//(used for position, on-destroy/collide/.. callbacks, ...
		
		int splitpos = arguments.find(" and ");
		std::vector < std::string> argumentsContainer;
		for (; splitpos != arguments.npos; splitpos = arguments.find(" and ")) {
			argumentsContainer.push_back(arguments.substr(0, splitpos));
			int endpos = 0;
			endpos = arguments.find_first_of(" ", splitpos + 1);
			arguments = arguments.substr(endpos+1, arguments.length() - endpos-1);
		}
		argumentsContainer.push_back(arguments);

		for (auto s : argumentsContainer) {
			std::string arg, params;
			arg = params = "";
			int pos = 0;
			pos = s.find_first_not_of(" ");
			if ((pos = s.find(" ", pos)) != s.npos) {
				arg = s.substr(0, pos);
				params = s.substr(pos+1, s.length() - pos-1);
			}
			else 
				continue;

			//KeyframeAnimation sets the animation that runs this object
			if (arg.find("keyframeanimation") != arg.npos) {
				//We overwrite the obj with the animation
				int beginpos = 0;
				beginpos = params.find_first_not_of(" ");
				KeyframeAnimation* newAnim = new KeyframeAnimation(params.substr(beginpos, params.length() - beginpos), false, false, newobj.m_object);
			}
			//At sets the position of the object
			else if (arg.find("at")!=arg.npos) {
				//Expecting syntax x,y, so split at ','
				float x, y;
				x = y = 0.0f;
				int splitpos = 0;
				if ((splitpos = params.find(",")) == params.npos) {
					std::cout << "ERROR MAP: invalid syntax " << s << std::endl;
					continue;
				}
				stringstream lstream(params.substr(0, splitpos));
				stringstream rstream(params.substr(splitpos + 1, params.length() - 1 - splitpos));
				lstream >> x;
				rstream >> y;
				obj->SetPosition(glm::vec2(x, y));
			}
			//size sets the size of the object
			else if (arg.find("size") != arg.npos) {
				//Expecting syntax x,y, so split at ','
				float x, y;
				x = y = 0.0f;
				int splitpos = 0;
				if ((splitpos = params.find(",")) == params.npos) {
					std::cout << "ERROR MAP: invalid syntax " << s << std::endl;
					continue;
				}
				stringstream lstream(params.substr(0, splitpos));
				stringstream rstream(params.substr(splitpos + 1, params.length() - 1 - splitpos));
				lstream >> x;
				rstream >> y;
				obj->SetSize(glm::vec2(x, y));
			}
			//Ondestroy sets the ondestroycallback
			else if (arg.find("ondestroy") != arg.npos) {
				int beginpos = 0;
				beginpos = params.find_first_not_of(" ");
				newobj.m_onDestroyBlock = params.substr(beginpos, params.length() - beginpos);
			}
			//OnCollide sets the oncollidecallback
			else if (arg.find("oncollide") != arg.npos) {
				int beginpos = 0;
				beginpos = params.find_first_not_of(" ");
				newobj.m_onCollideBlock = params.substr(beginpos, params.length() - beginpos);
			}
			//Ondamage sets the ondamagecallback
			else if (arg.find("ondamage") != arg.npos) {
				int beginpos = 0;
				beginpos = params.find_first_not_of(" ");
				newobj.m_onDamageBlock = params.substr(beginpos, params.length() - beginpos);
			}
			else if (arg.find("donttrack") != arg.npos) { 
				result = (params.find("false")!=params.npos);
			}
			
		}

		
	}
	
	return result;

}

//Here all type "strings" get connected with the actual argument paresrs and constructors
void MapObject::Setup()
{
	MAPTYPE_BEGIN
	MAPTYPE(Player)
	MAPTYPE(Sprite)
	MAPTYPE(NPCFighter)
	MAPTYPE_END
}