#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D textureSampler;
uniform float alpha = 1;

void main(){

	color = vec4(texture2D( textureSampler, UV ).rgba);


}