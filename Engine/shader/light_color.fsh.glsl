 #version 330 core

uniform float strength;
uniform vec3 incolor;
uniform float radius;
uniform vec2 position;

// Interpolated values from the vertex shaders
in vec2 UV;
in float dist;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D textureSampler;
uniform float alpha = 1;

void main(){
	vec2 lightdir = vec2(UV.x, UV.y)-position.xy;
	float dist = sqrt(lightdir.x*lightdir.x+lightdir.y*lightdir.y);
	vec3 basecolor = texture2D( textureSampler, UV ).rgb;
	float factor = 0;
	if(dist<radius)
		factor = ((radius-dist)/radius)*strength;

	color = vec4(basecolor + basecolor*incolor*factor, 1.0);


}