#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_screenspace;



// Output data ; will be interpolated for each fragment.
out vec2 UV;

void main(){
float xscale = (vertexPosition_screenspace.x+1)/2;
float yscale = (vertexPosition_screenspace.y+1)/2;


gl_Position = vec4(vertexPosition_screenspace.xy, 0.0,1.0);


UV = vec2(xscale, yscale);
}

