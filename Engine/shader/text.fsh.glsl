 #version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D TextureSampler;
uniform vec4 text_color;


void main(){
	color = vec4(1.0, 1.0, 1.0, texture2D(TextureSampler, UV).r)*text_color;
}