#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_screenspace;

uniform vec2 position;
uniform vec4 offset;
uniform vec2 size;
uniform float layer = 0;
// Output data ; will be interpolated for each fragment.
out vec2 UV;

void main(){
float xscale = (vertexPosition_screenspace.x+1)/2;
float yscale = (vertexPosition_screenspace.y+1)/2;

vec2 inpos = vec2(position.x*2-1 + xscale*2*size.x, (position.y)*2-1+yscale*2*size.y);
gl_Position = vec4(inpos, 0.1*layer,1.0);

// UV of the vertex. No special space for this one.
float w = offset.z - offset.x;
float h = offset.w - offset.y;

UV = vec2(offset.x+w*xscale, 1-(offset.y+h*yscale));

}

